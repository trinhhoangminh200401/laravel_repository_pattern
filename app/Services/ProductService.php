<?php

namespace App\Services;

use App\Http\constant\Util;
use App\Interface\IRepository\IProductRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ProductService
{

    protected $productRepository;

    public function __construct(IProductRepository $productRepository)
    {

        $this->productRepository = $productRepository;
    }

    // public function getlimitProduct(): Collection
    // {

    //     return $this->productRepository->getlimitProduct();
    // }


    // public function getAllProducts($param = '')
    // {
    //     // $products = $this->productRepository->getAll();
    //     $products = $this->productRepository->search($param);

    //     $listProduct = [];
    //     foreach ($products as  $value) {

    //         $listProduct[] = $value;
    //     }
    //     $productDatas = array_map(function ($element) {
    //         if (!empty($element->image)) {

    //             $dataImage  = Util::getListImageFromString($element->image);

    //             if (is_array($dataImage) && count($dataImage) > 0) {

    //                 $element->image  =  $dataImage[0];

    //                 return  $element;
    //             }
    //             return $element;
    //         } else {


    //             return  $element;
    //         }
    //     }, $listProduct);


    //     return response()->json(['data' => $productDatas, 'messsage' => 'success'], 200);
    // }


    // public function getProductPage($currentPage, $perPage, $param = '')
    // {


    //     if (!empty($param)) {

    //         $allData = $this->productRepository->search($param);

    //         $count = $allData->count();

    //         $perPage = $perPage > 0 ? $perPage  : $count;

    //         $offset = ($currentPage - 1) * $perPage;


    //         $data = $allData->slice($offset, $perPage)->all();
    //     } else {

    //         $count = $this->productRepository->getAllCountProduct();

    //         $offset = ($currentPage - 1) * $perPage;

    //         $data = $this->productRepository->paginationData($offset, $perPage);
    //     }

    //     $totalPages = $perPage > 0 ? ceil($count / $perPage) : 0;


    //     $listProduct = [];

    //     foreach ($data as  $value) {

    //         $listProduct[] = $value;
    //     }

    //     $productDatas = array_map(function ($element) {
    //         if (!empty($element->image)) {

    //             $dataImage = Util::getListImageFromString($element->image);

    //             if (is_array($dataImage) && count($dataImage) > 0) {

    //                 $element->image = $dataImage[0];
    //             }

    //             return  $element;
    //         } else {
    //             return  $element;
    //         }
    //     }, $listProduct);

    //     // if (count($productDatas) <= 0) {

    //     //     return false;
    //     // }

    //     $data = [


    //         'response' => $productDatas,

    //         'currentPage' => $currentPage > 0 ? $currentPage : 0,

    //         'pageItemNumber' => $perPage > 0 ? $perPage : 0,

    //         'totalPages' => $totalPages,

    //         'totalProducts' => $count,

    //     ];
    //     return  $data;
    // }


    // public function getList($input, $options){

    //     return $this->productRepository->getList($input,$options);

    // }


    public function getPaginatedProducts($filters = [], $limit = '', $page = '',$options)
    {
        // $cacheKey = 'products_' . implode('_', $filters) . "_page_{$page}_limit_{$limit}";
        // return Cache::remember(
        //     $cacheKey,
        //     3600*24*7*30,
        //     function () use ($filters, $limit, $page) {
        // //     return $this->productRepository->getAllProducts($filters, $limit, $page);
        // $startTime = microtime(true);

        // $this->productRepository->getAllProducts($filters, $limit, $page);

        // $endTime = microtime(true);

        // $timeExcute = $endTime - $startTime;
        // $timeExcute=2;
        // if ($timeExcute > (float)1) {
        //     $cacheKey = 'products.list.page.' . md5(json_encode($filters) . $limit . $page);
        //    return  Cache::store('redis')->remember(
        //         $cacheKey,
        //         3600 * 60,
        //         function () use ($filters, $limit, $page) {
        //             return $this->productRepository->getAllProducts($filters, $limit, $page);
        //         }
        //     );

          return $this->productRepository->getAllProducts($filters, $limit, $page,$options);

        // } else {
        //     return  $this->productRepository->getAllProducts($filters, $limit, $page);
        // }


        //      } );
        // DB::enableQueryLog();

        // dd(DB::getQueryLog());
    }

    public function getTopProducts($filters = [], $limit = '', $page = '',$options)
    {

        return $this->productRepository->getTopProducts($filters, $limit, $page,$options);

    }
    public function insertProduct($request)
    {

        $data = $request->all();


        return $this->productRepository->create($data);
    }

    public function updateProduct($request, $id)
    {

        $data = $request->all();

        if (!is_numeric($id)) {

            return response()->json([

                'message' => 'The id is uncorrect type',

                'data' => []

            ], 415);
        };
        $result = $this->productRepository->update($id, $data);

        return $result ? response()->json([

            'message' => "update successfully!",

            'status' => 202,

            'data' => $result

        ], 202) :
            response()->json([

                'message' => "Not Found Id To handle !",

                'status' => 500,

                'data' => $result

            ], 500);
    }
    public function getProductId($id)
    {
        if (!is_numeric($id)) {

            return response()->json(['message' => 'type of id is not true '], 404);
        } else {

            $data = $this->productRepository->find($id);

            if ($data) {

                return response()->json([

                    'data' => $data,

                    'message' => 'Success to fetch current Product',

                    'status' => 200


                ]);
            }
            return response()->json(['message' => 'Can not find id Product'], 404);
        }
    }
    // public function searchProduct($param)
    // {

    //     $data = $this->productRepository->search($param);


    //     if (count($data) != 0) {

    //         return  response()->json([

    //             'status'=>200,

    //             'data'=> $data,

    //             'message'=>'Search Successfully'

    //         ],200)

    //      ;
    //     }

    // return response()->json(['message' => 'Can not find info in Product', 'status' => 404,'data'=>$data], 404);
    // }

    public function destroyProduct($id)
    {


        return $this->productRepository->destroy($id) ? response()->json([

            'messsage' => 'Delete Success',

            'status' => 202,

        ], 202) :

            response()->json([

                'messsage' => 'Record is block!',

                'status' => 404,

            ], 404);
    }
    public function deleteProduct($id)
    {

        return $this->productRepository->deleted($id) ? response()->json([

            'message' => 'Complete Delete in Record',

            'status' => 204,

        ], 204) :
            response()->json([

                'message' => 'Record is not exist',

                'status' => 404,

            ], 404);
    }
    public function mulipleDeleteProduct($request)
    {

        return $this->productRepository->multipleDestroy($request->input('ids', []));
    }
}
