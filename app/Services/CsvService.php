<?php

namespace App\Services;

use App\InterFace\IServices\ICsvService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CsvService implements ICsvService
{
    public function exportFileCsv($data = [], $headers = [], $fileName = 'data.csv')
    {
        $response = new StreamedResponse(function () use ($data, $headers) {
            // dd($data['data']);
            $handle = fopen('php://output', 'w');

            fputcsv($handle, $headers);
            foreach ($data['data'] as $row) {
                $csvRow = [];
                foreach ($headers as $header) {
                    $csvRow[] = isset($row[$header]) ? $row[$header] : 'null';
                }
                fputcsv($handle, $csvRow);
            }

            fclose($handle);
        });

        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename=' . $fileName);

        return $response;
    }

    public function importFileCsv($data = [], $model)
    {
        $file = $data['file'];

        try {
            $filePath = $file->getRealPath();
            $fileOpen = fopen($filePath, 'r+');
            $header = fgetcsv($fileOpen);

            $escapedHeader = [];
            
            foreach ($header as $key => $value) {
                $lheader = strtolower($value);
                $escapedItem = preg_replace('/[^a-z]/', '', $lheader);
                array_push($escapedHeader, $escapedItem);
            }

            DB::beginTransaction();

            while ($columns = fgetcsv($fileOpen)) {
                if (count($columns) == count($escapedHeader)) {
                    $data = array_combine($escapedHeader, $columns);

                    $model::updateOrCreate(
                        ['name' => $data['name']],
                    );
                } else {
                    throw new \Exception("CSV structure is incorrect");
                }
            }

            DB::commit();
        } catch (\Exception $th) {
            DB::rollBack();
            Log::error('Error importing CSV', ['error' => $th->getMessage()]);
            throw $th;
        }
    }
}
