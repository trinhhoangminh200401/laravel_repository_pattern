<?php
namespace  App\Services\AuthService;
use App\InterFace\IServices\IEmailService;
use App\InterFace\IServices\IResponsewithToken;
use App\Mail\OtpMail;
use App\Models\Customer;
use App\Models\User;
use App\Services\AuthService\AuthenticateService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;

class EmailService extends AuthenticateService  implements IEmailService
{

    public function register($request=[])
    { {

            $existUser = Customer::where('email', '=', $request['email'])->first();
        // can't create new account when this email is exist and verified =1
            if ($existUser && $existUser->isVerified == 1) {

                // DB::enableQueryLog();

                // $existUser;

                // dd(DB::getQueryLog());

                return response()->json([

                    'status' => 409,

                    'message' => 'Email is already'


                ], 409);
            }
        //  Override account when this account isVerified =0
            if($existUser && $existUser->isVerified == 0){

                 $existUser->update([

                    'name' => $request['name'],

                    'email' => $request['email'],

                    'password' => Hash::make($request['password'])


                ]);
            }


            if (!$existUser) {

                $user = Customer::create([

                    'name' => $request['name'],

                    'email' => $request['email'],

                    'password' => Hash::make($request['password'])


                ]);

            }
            else{
                $user=$existUser;

                $otp = rand(100000, 999999);


                $cachedOtp = Cache::get('otp_' . $user->id);

                if ($cachedOtp) {
                    return response()->json([

                        'status' => 'false',

                        'message' => 'Your Otp is still exist!'

                    ]);
                }

                Cache::put('otp_' . $user->id, $otp, now()->addMinutes(5));

                Mail::to($user->email)

                    ->send(new OtpMail($otp));

                return response()->json([

                    'status' => 200,

                    'message' => 'OTP has been sent to your email'

                ], 200);
            }


        }
    }
    public function verifyOtpEmail($request)
    {

        $request->validate([

            'email' => 'required|email|exists:customers,email',

            'otp' => 'required|integer',

        ]);

        $user = Customer::where('email', $request->email)->first();

        $cachedOtp = Cache::get('otp_' . $user->id);


        if ($cachedOtp && $cachedOtp == $request->otp) {


            $user->isVerified = 1;

            $user->save();

            Cache::forget('otp_' . $user->id);

            $token = Auth::login($user);

            $refreshToken = $this->createRefreshToken();

            return $this->respondWithToken($token, $refreshToken);
        }

        return response()->json([

            'status' => 400,

            'message' => 'Invalid or expired OTP'

        ], 401);
    }

}
