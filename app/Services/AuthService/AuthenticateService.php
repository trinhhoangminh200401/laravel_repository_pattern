<?php

namespace App\Services\AuthService;

use Illuminate\Support\Facades\Auth;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;
use App\Http\Traits\ApiResponse;

class AuthenticateService
{
    use ApiResponse;
    protected function createRefreshToken()
    {
        $data = [

            'sub' => Auth::user()->id,

            'random' => rand() . time(),

            'exp' => time() + config('jwt.refresh_ttl')
        ];
        return JWTAuth::getJWTProvider()->encode($data);
    }

    protected function respondWithToken($token, $refresh)
    {
        $data = [
            'access_token' => $token,
            'refresh_token' => $refresh,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60 * 24 * 7,
        ];

        return $this->apiResponse($data, 200, 'Token generated successfully', 200);
    }
}
