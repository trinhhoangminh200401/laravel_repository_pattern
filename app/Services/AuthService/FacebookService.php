<?php

namespace  App\Services\AuthService;

use App\InterFace\IServices\ISocialService;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;


class FaceBookService extends AuthenticateService implements ISocialService
{

    public function redirectToProvider($provider)
    {
        Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        try {
            $FacebookUser = Socialite::driver($provider)->user();

            $userEmail = Customer::where("email", $FacebookUser->getEmail())->first();

            if (!$userEmail) {

                $userEmail = Customer::create([

                    "name" => $FacebookUser->getName(),

                    "email" => $FacebookUser->getEmail(),

                    "password" => $FacebookUser->Hash::make(uniqid())
                ]);
            }

            $token = Auth::login($FacebookUser);

            $refreshToken = $this->createRefreshToken();

            return $this->respondWithToken($token, $refreshToken);
        } catch (\Exception $e) {

            Log::error('Failed to authenticate using Facebook: ' . $e->getMessage());

            return response()->json(['error' => 'Failed to authenticate using Facebook'], 500);
        }
    }
}
