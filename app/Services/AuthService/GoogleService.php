<?php

namespace App\Services\AuthService;



use App\InterFace\IServices\ISocialService;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;


class GoogleService extends AuthenticateService implements ISocialService
{
    public function redirectToProvider($provider)
    {
        $client = request()->get('client');

        Cache::put('client_key', $client);


        return  response()->json(
            [
                'url' => Socialite::driver($provider)->stateless()->scopes(['email', 'profile'])
                    ->with(['prompt' => 'consent'])->redirect()->getTargetUrl()
            ]
        );
    }

    public function handleProviderCallback($provider)
    {
        try {
            $googleUser = Socialite::driver($provider)->stateless()->user();

            $clientUrl = Cache::get('client_key');

            $user = Customer::where('email', $googleUser->getEmail())->first();

            if (!$user) {
                $user = Customer::create([

                    'name' => $googleUser->getName(),

                    'email' => $googleUser->getEmail(),

                    'password' => Hash::make(uniqid()),

                    'isVerified'=> $googleUser->user['verified_email'] == true ? 1 : 0,

                    'image'=> $googleUser->user['picture']
                ]);
            }
            if ($user && $user->isBlocked == 1) {

                return redirect()->to("{$clientUrl}/auth/callback?status=403&message=" . urlencode('Your Account was blocked'));

            }
            if($user && $user->isVerified == 0){

                return redirect()->to("{$clientUrl}/auth/callback?status=401&message=" . urlencode('Your Account was blocked'));

            }

            $token = Auth::login($user);


            $refreshToken = $this->createRefreshToken();


            return redirect()->to("{$clientUrl}/auth/callback?token={$token}&refreshToken={$refreshToken}&status=200&message=". urlencode('Login success'));


            // return $this->respondWithToken($token, $refreshToken);
        } catch (\Exception $e) {

            Log::error('Failed to authenticate using Google: ' . $e->getMessage());

            return response()->json(['error' => 'Failed to authenticate using Google'], 500);
        }
    }
}
