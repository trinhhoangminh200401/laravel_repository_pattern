<?php

namespace App\Services;

use App\Interface\IRepository\IUserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserService
{

    protected $userRepository;

    public function __construct(IUserRepository $userRepository)
    {

        $this->userRepository = $userRepository;
    }

    public function findByEmail($email)
    {

        return $this->userRepository->findByEmail($email);
    }
    public function  getUserId($request){

        return $this->userRepository->find($request->input('id'));

    }
    public function getAllUsers($filters=[],$limit='',$page='',$options=[]){

        return $this->userRepository->getAllUsers($filters,$limit,$page,$options);

    }
    public function updateUser($request, $id)
    {

        // $isRole = Auth::user();

        if (!is_numeric($id)) {

              return [

                'status'=>false,

                'message'=> "User id is not correct",

                'status_code'=>400
              ];

        };

        $condition = $request;

        $result = $this->userRepository->update($id, $condition);
        if(!$result)
        {

            return [

                'status' => false,

                'message' => "User not found or failed to update",

                'status_code' => 404,

            ];

        }

          return [
            'status'=>true,

            'message'=>'update successfully!',

            'data'=>$result,

            'status_code' => 200,
          ];

        // if ($isRole == 'admin') {

        //     $condition = $request->all();


        //     $result = $this->userRepository->update($id, $condition);
        // }
        // if ($isRole != 'admin') {

        //     if ($request->input('role')) {

        //         return response()->json([

        //             "status" => 401,

        //             "message" => "Unauthourize to change role"

        //         ], 401);
        //     } else {

        //         $condition = $request->all();

        //         $result = $this->userRepository->update($id, $condition);
        //     }
        // }



    }

    // public function getAllUser()
    // {

    //     return $this->userRepository->getAll();
    // }
    public function blockUser($id)

    {

        if(!is_numeric($id)){

            return response()->json([

                'status'=>400,

                'message'=> 'Yours id is in correct type!'

            ]);

        }

        $CheckBlocked=$this->userRepository->find($id);

        $blockUser=  $this->userRepository->blockUser($id);

        if(!$blockUser){

            return response()->json([

                'status'=>404,

                'message'=>'Your current user has already deleted or not been exist!'

            ],404);
        }

        if($CheckBlocked->isBlocked){

            return response()->json([

                'status' => 404,

                'message' => 'Your current user has already blocked!'

            ], 404);
        }
        return response()->json([

            'status'=>202,
            'message'=>'Success to Block User with '. $id

        ]);
    }

    public function blockMultiple($request){

        if (!$request->has('user_ids')) {
            return [
                'status' => false,
                'message' => 'User IDs are required'
            ];
        }

        $userIds = $request->input('user_ids');

        if (empty($userIds) || !is_array($userIds)) {
            return [
                'status' => false,
                'message' => 'Invalid user IDs'
            ];
        }
        $result = $this->userRepository->blockMultiple($userIds);
        if ($result) {
            return [
                'status' => true,
                'message' => 'Users blocked successfully'
            ];
        }

        return [
            'status' => false,
            'message' => 'Failed to block users'
        ];
    }



}
