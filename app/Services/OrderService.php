<?php
namespace App\Services;

use App\Interface\IRepository\IOrderRepository;
use App\Interface\IUnitOfWork\IUnitOfWork;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderService   {

    private $unitOfwork;
    private $orderUnitOfWork;

    public function __construct(IUnitOfWork $unitOfWork)
    {

        $this->unitOfwork=$unitOfWork;

        $this->orderUnitOfWork=$this->unitOfwork->getRepository(IOrderRepository::class);

    }
    public function getOrderList ($options ='',$request){


            $limit = $request->input('limit','');

            $page =$request->input('page','');

            return $this->orderUnitOfWork->getAllOrder($options,$limit,$page);

    }
    public function createOrder($request){
        try {
            
            $inputs = [

                'customer_id' => $request->input('customer_id'),

                'products' => $request->input('products', []),

                'combos' => $request->input('combos', []),
            ];
             return $this->orderUnitOfWork->createOrder($inputs);

        } catch (\Exception $err) {

            Log::info($err->getMessage());

        }


    }

}

?>
