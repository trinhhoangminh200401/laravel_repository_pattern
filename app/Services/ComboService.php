<?php

namespace App\Services;

use App\Http\constant\Util;
use App\Interface\IRepository\IComboRepository;

class ComboService
{

    private $comboProductRepository;

    public function __construct(IComboRepository $comboProductRepository)
    {
        $this->comboProductRepository = $comboProductRepository;
    }

    public function getAllCombo($filters =[], $limit='',$page='',$options=[])
    {

        return $this->comboProductRepository->getAllCombos($filters,$limit,$page,$options);

    }
    public function insertComboProduct($inputs)
    {

        return $this->comboProductRepository->createCombo($inputs);
    }
    public function getComboId($id)
    {
        $comBoName = Util::getModelName($this->comboProductRepository->getModel());
        if (!is_numeric($id)) {
            return response()->json(['success' => "false", 'message' => 'Type id of' . " " . $comBoName . " " . 'is not true '], 400);
        } else {
            $data = $this->comboProductRepository->find($id);

            if ($data) {

                return response()->json([
                    'data' => $data,
                    'message' => 'success'
                ]);
            }
            return response()->json(['success' => "false", 'message' => 'Can not find id' . " " . $comBoName,'data'=>$data==NULL ? [] :''], 404);
        }
    }
    public function deleteCombo ($id){

        return $this->comboProductRepository->deleted($id) ? response()->json([

            'message' => 'Complete Delete in Record',

            'status' => 204,

        ], 204) :
            response()->json([

                'message' => 'Record is not exist',

                'status' => 404,

            ], 404);
    }
    public function destroyCombo ($id){


        return $this->comboProductRepository->destroy($id) ? response()->json([

            'messsage' => 'Delete Success',

            'status' => 202,

        ], 202) :

            response()->json([

                'messsage' => 'Record is block!',

                'status' => 404,

            ], 404);

    }
}
