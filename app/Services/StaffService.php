<?php

namespace App\Services;

use App\Interface\IRepository\IStaffRepository;
use App\Models\Staff;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class StaffService
{

    protected IstaffRepository $staffRepository;

    public function __construct(IStaffRepository $staffRepository)
    {

        $this->staffRepository = $staffRepository;
    }
    public function getAllStaff($filters = [], $limit = 10, $page = 1): array
    {
        return $this->staffRepository->getAllStaff($filters, $limit, $page);
    }


    public function getStaffById($id)
    {
        return $this->staffRepository->findById($id, ['isDeleted' => false]);
    }

    public function getIdFromStaff($id){

        return $this->staffRepository->find($id)->where('isDeleted','0');

    }



    public function createStaff($data){
        return $this->staffRepository->create($data);

    }
    public function updateStaff($request, $id) {
        // Step 1: Validate the incoming data (optional fields, only validating if provided)
        $validator = Validator::make($request->all(), [
            'name' => 'nullable|string|max:255',
            'phone_number' => 'nullable|string|max:15',
            'role' => 'nullable|string|max:50',
            'isBlocked' => 'nullable|integer', // Đổi từ 'number' sang 'integer'
            'isDeleted' => 'nullable|boolean',
            'image' => 'nullable|string', // Hoặc thay bằng kiểu file nếu bạn tải lên file ảnh
        ]);


        // Step 2: Handle validation failures
        if ($validator->fails()) {
            return [
                'status' => false,
                'message' => 'Validation error',
                'errors' => $validator->errors(),
            ];
        }

        try {
            // Step 3: Find the staff member by ID
            $staff = Staff::find($id);

            // Step 4: Handle if staff is not found
            if (!$staff) {
                return [
                    'status' => false,
                    'message' => 'Staff not found',
                ];
            }

            // Step 5: Exclude fields that should not be updated (email, password, id)
            $dataToUpdate = $request->except(['email', 'password', 'id']);
          //  dd($dataToUpdate);
            // Step 6: Update staff with allowed fields
            $staff->update($dataToUpdate);

            // Step 7: Return the updated staff data
            return [
                'status' => true,
                'message' => 'Staff updated successfully',
                'data' => $staff,
            ];

        } catch (\Exception $e) {
            // Step 8: Catch any exception that occurs during the process
            return [
                'status' => false,
                'message' => 'An error occurred while updating the staff member',
                'error' => $e->getMessage(),
            ];
        }
    }
    public function deleteStaff($id): bool
    {
        try {
            // Step 1: Find the staff member by ID
            $staff = Staff::find($id);

            // Step 2: Handle if staff is not found
            if (!$staff) {
                return false; // Indicates the staff member was not found
            }

            // Step 3: Soft delete the staff member
            // Assuming you're using soft deletes with `isDeleted` flag
            $staff->isDeleted = true;
            $staff->save();

            // If you are using Laravel's built-in soft delete feature, use this instead:
            // $staff->delete();

            return true; // Indicates successful deletion
        } catch (\Exception $e) {
            // Handle any errors during the deletion process
            return false; // Failure during deletion
        }
    }
    public function findByEmail($email)
    {

        return $this->staffRepository->findByEmail($email);
    }

    public function updateUser($request, $id)
    {

        $isRole = Auth::user()->role;

        if (!is_numeric($id)) {

            return response()->json([

                'message' => 'The id is uncorrect type'

            ], 415);
        };


        if ($isRole == 'admin') {

            $condition = $request->all();


            $result = $this->staffRepository->update($id, $condition);
        }
        if ($isRole != 'admin') {

            if ($request->input('role')) {

                return response()->json([

                    "status" => 401,

                    "message" => "Unauthourize to change role"

                ], 401);
            } else {

                $condition = $request->all();

                $result = $this->staffRepository->update($id, $condition);
            }
        }

        return $result ? response()->json([

            'message' => "Update Successfully!",

            'status' => 202,

            'data' => $result

        ], 202) :
            response()->json([

                'message' => "To handle fail !",

                'status' => 500,

                'data' => $result

            ], 500);
    }
    public function getAllUser()
    {

        return $this->staffRepository->getAll()->get();
    }
    public function blockUser($id)

    {

        if(!is_numeric($id)){

            return response()->json([

                'status'=>400,

                'message'=> 'Yours id is in correct type!'

            ]);

        }

        $CheckBlocked=$this->staffRepository->find($id);

        $blockUser=  $this->staffRepository->blockUser($id);

        if(!$blockUser){

            return response()->json([

                'status'=>404,

                'message'=>'Your current user has already deleted or not been exist!'

            ],404);
        }

        if($CheckBlocked->isBlocked){

            return response()->json([

                'status' => 404,

                'message' => 'Your current user has already blocked!'

            ], 404);
        }
        return response()->json([

            'status'=>202,
            'message'=>'Success to Block User with '. $id

        ]);
    }

    public function blockMultiple($request){

        if (!$request->has('user_ids')) {
            return [
                'status' => false,
                'message' => 'User IDs are required'
            ];
        }

        $userIds = $request->input('user_ids');

        if (empty($userIds) || !is_array($userIds)) {
            return [
                'status' => false,
                'message' => 'Invalid user IDs'
            ];
        }
        $result = $this->staffRepository->blockMultiple($userIds);
        if ($result) {
            return [
                'status' => true,
                'message' => 'Users blocked successfully'
            ];
        }

        return [
            'status' => false,
            'message' => 'Failed to block users'
        ];
    }



}
