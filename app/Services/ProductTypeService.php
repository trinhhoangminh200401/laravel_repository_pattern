<?php

namespace App\Services;

use App\Http\constant\Util;
use App\Http\Traits\ApiResponse;
use App\Interface\IRepository\IProductRepository;
use App\Interface\IRepository\IProductTypeRepository;
use Illuminate\Support\Facades\DB;

class ProductTypeService
{

    use ApiResponse;
    private $productTypeRepository;

    public function __construct(IProductTypeRepository $productTypeRepository, )
    {
        $this->productTypeRepository = $productTypeRepository;
    }
    // public function getListBelongTypes($id, $currentPage = null, $perPage = null, $search = '')
    // {


    //     if (!isset($id)) {

    //         $message = [
    //             'status' => 426,

    //             'message' => 'Missing required parameter: type',

    //             'data' => []

    //         ];
    //         return response()->json($message, 426);
    //     }

    //     $listProductCategories = $this->productTypeRepository->getListBelongType($id);

    //     $result = [];

    //     if (!$listProductCategories) {


    //         $offset = $perPage;


    //         return $this->productList->getProductPage($offset, $perPage);
    //     } else {

    //         foreach ($listProductCategories as  $productType) {

    //             if (!empty($search)) {

    //                 $dataRelation = Util::searchModel($productType->product(), $search);

    //                 $totalProducts = $dataRelation->count();
    //             } else {

    //                 $dataRelation = $totalProducts = $productType->product();

    //                 $totalProducts = $productType->product()->count();
    //             }
    //             $perPage = empty($perPage) ?  $productType->product()->count() : $perPage;


    //             $offset = ($currentPage - 1) * $perPage;

    //             // dd($productType->product());

    //             // dd($perPage,$offset,   $pageNumber);

    //             $totalPages = ceil($totalProducts / $perPage);

    //             $paginatedProducts = $this->productTypeRepository->paginationData($offset, $perPage,  $dataRelation);



    //             if (count($paginatedProducts) > 0) {

    //                 foreach ($paginatedProducts as  $value) {

    //                     $data = Util::getListImageFromString($value->image);


    //                     $value->image =  is_array($value->image) ? $data[0] : $value->image;
    //                 }
    //             }

    //             $result[] =

    //                 [
    //                     'id' => $productType->id,

    //                     'name'  => $productType->name,

    //                     'image' => $productType->Image,

    //                     'products' => $paginatedProducts,

    //                     'pagination'   => count($paginatedProducts) > 0 ? [

    //                         'pageItemNumber' => $perPage,

    //                         'currentPage'  =>  $currentPage > 0 ? $currentPage : '1',

    //                         'totalPages' => $totalPages,

    //                         'totalProducts' => $totalProducts,
    //                     ] : []

    //                 ];
    //         }
    //     }

    //     return response()->json(
    //         [
    //             'message' => 'success',

    //             'status' => 200,

    //             'data' => $result
    //         ],
    //         200
    //     );
    // }
    public function getAllType($filters=[],$limit='', $page = '',$options=[]){

        return $this->productTypeRepository->getALlType($filters, $limit,$page,$options);
    }

    public function getBelongTypeIdProduct($id,$currentPage='',$perPage='',$search='',$options=[])
    {

            return $this->productTypeRepository->getProductListBelongToType($id, $currentPage, $perPage, $search, $options);

    }
    public function getTypeById($request){

        return $this->productTypeRepository->find($request);
    }

    public function insertProductType($request)
    {

        return $this->productTypeRepository->create($request);
    }
    public function multipleDestroyProductType($request){

        return $this->productTypeRepository->multipleDestroy($request->input('ids', []));
    }
}
