<?php

namespace App\Providers;

use App\Interface\IRepository\IStaffRepository;
use App\Repositories\StaffRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        // $this->app->singleton(


        // );
        $this->app->register(DalServiceProvider::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
