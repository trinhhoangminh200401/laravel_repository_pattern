<?php

namespace App\Providers;

use App\InterFace\IServices\ISocialService;
use App\Services\AuthService\FaceBookService;
use App\Services\AuthService\GoogleService;
use Illuminate\Support\ServiceProvider;

class DalServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $toBind = [

            \App\Interface\IRepository\IBaseRepository::class => \App\Repositories\Repository\BaseRepository::class,

            \App\Interface\IRepository\IProductRepository::class => \App\Repositories\Repository\ProductRepository::class,

            \App\Interface\IRepository\IUserRepository::class => \App\Repositories\Repository\UserRepository::class,

            \App\Interface\IRepository\IStaffRepository::class => \App\Repositories\Repository\StaffRepository::class,

            \App\Interface\IRepository\IProductTypeRepository::class => \App\Repositories\Repository\ProductTypeRepository::class,

            \App\Interface\IRepository\IComboRepository::class => \App\Repositories\Repository\ComboRepository::class,

            \App\Interface\IRepository\IOrderRepository::class => \App\Repositories\Repository\OrderRepository::class,

            // \App\Interface\IRepository\IAuthRepository::class => \App\Repositories\Repository\AuthRepository::class,
            \App\Interface\IRepository\IOrderItemRepository::class=>\App\Repositories\Repository\OrderItemRepository::class,

            \App\Interface\IRepository\IOrderComboRepository::class => \App\Repositories\Repository\OrderComboRepository::class,

            \App\InterFace\IServices\IEmailService::class => \App\Services\AuthService\EmailService::class,

            \App\InterFace\IServices\ICsvService::class => \App\Services\CsvService::class,

            \App\Interface\IUnitOfWork\IUnitOfWork::class=>\App\Repositories\UnitOfWork\UnitOfWork::class

        ];
        foreach ($toBind as $interface => $implementTation) {

            $this->app->bind($interface, $implementTation);
        }
        $this->app->when(\App\Http\Controllers\AuthController::class)

            ->needs(ISocialService::class)

            ->give(function ($app) {

                if (request()->is('*/facebook/*')) {

                    return $app->make(FaceBookService::class);
                }

                return $app->make(GoogleService::class);
            });
        $this->app->when(\App\Http\Controllers\AuthAdminController::class)

            ->needs(\App\Interface\IRepository\IAuthRepository::class)

            ->give(\App\Repositories\Repository\AuthAdminRepository::class);

        $this->app->when(\App\Http\Controllers\AuthController::class)

                ->needs(\App\Interface\IRepository\IAuthRepository::class)

                ->give(\App\Repositories\Repository\AuthRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
