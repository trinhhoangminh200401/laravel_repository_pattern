<?php

namespace App\Interface\IRepository;



use App\Interface\IRepository\IBaseRepository ;

interface IStaffRepository extends IBaseRepository{

    public function findByEmail($email);
    
    public function findById($id,$conditions = array());

    public function blockUser($id);

    public function blockMultiple($request);

}

?>
