<?php

namespace App\Interface\IRepository;

use App\Interface\IRepository\IBaseRepository;

interface IProductTypeRepository extends IBaseRepository{

    public function getListBelongType($id);

    public function getProductListBelongToType($id,$currentPage=1,$perPage=10, $filters = []);

    public function getAllType($filters=[], $limit = '',$page='',$options=[]);


}

