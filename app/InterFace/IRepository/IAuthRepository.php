<?php
 namespace App\Interface\IRepository;

 interface IAuthRepository{

    public function Authenticate($credential=[]);
    public function logout();
    public function refreshToken($request);
    public function profile();

 }
?>
