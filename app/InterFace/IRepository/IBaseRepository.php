<?php

namespace App\Interface\IRepository;



interface IBaseRepository
{
    public function find($id);
    public function getSize();
    public function getAll();
    public function create($attributes = []);
    public function update($id, $attributes = []);
    public function destroy($id);
    public function deleted($id);
    // public function searchOptions($query,$options=[]);
    public function multipleDestroy($data=[]);
    public function paginationData($offset,$limit,$relation=null,$query=null);
}
