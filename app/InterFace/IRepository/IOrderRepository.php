<?php

namespace App\Interface\IRepository;

interface IOrderRepository extends IBaseRepository
{
    // Define your methods here
    public function getAllOrder($filters=[],$limit='',$page='');

    public function createOrder ($inputs=[]);
}
