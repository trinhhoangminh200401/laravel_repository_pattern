<?php

namespace App\Interface\IRepository;

use App\Interface\IRepository\IBaseRepository;
use Illuminate\Database\Eloquent\Collection;

interface  IProductRepository extends IBaseRepository{


    public function getAllProducts($filters = [], $limit = '', $page = '',$options=[]);
    public function getTopProducts($filters = [], $limit = '', $page = '',$options=[]);
    public function getProductWithType():Collection;


}
?>
