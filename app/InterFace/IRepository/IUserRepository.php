<?php

namespace App\Interface\IRepository;



use App\Interface\IRepository\IBaseRepository ;

interface IUserRepository extends IBaseRepository{

    public function findByEmail($email);

    public function blockUser($id);

    public function blockMultiple($request);

    public function getAllUsers($filters=[],$limit='',$page='',$options=[]);

}

?>
