<?php
namespace App\Interface\IRepository;

interface IComboRepository extends IBaseRepository{

    public function getAllCombos($filters = [], $limit = '', $page = '', $options = []);

    public function createCombo($inputs=[]);

}

?>
