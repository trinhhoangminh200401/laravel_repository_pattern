<?php

namespace App\InterFace\IServices;

interface IEmailService
{
    public function register(array $request);
    public function verifyOtpEmail($request);
}


