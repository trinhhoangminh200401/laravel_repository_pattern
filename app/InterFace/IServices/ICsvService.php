<?php

namespace App\InterFace\IServices;

   interface ICsvService{
        public function importFileCsv($data=[],$model);
        public function exportFileCsv($data=[],$filename,$header=[]);
   }

?>
