<?php

namespace App\InterFace\IServices;

interface ISocialService
{

    public function redirectToProvider($provider);
    public function handleProviderCallback($provider);
}
