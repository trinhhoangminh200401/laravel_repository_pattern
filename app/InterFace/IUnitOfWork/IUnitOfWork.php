<?php
namespace App\Interface\IUnitOfWork;
// <!-- this code is pending and handling  -->
// Unit of work is used when we handle multi action interact with insert, update, delete have to handle transaction to
// ensure that your data is consistency
interface IUnitOfWork{
    public function beginTransaction();
    public function commit();
    public function rollback();
    public function getRepository(string $repositoryClass);

}
?>

