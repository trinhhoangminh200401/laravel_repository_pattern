<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComboProduct extends Model
{
    use HasFactory;

    protected $table="combo_products";

    protected $timestamp = true;

    protected $primaryKey = 'id';

    protected $fillable=[

        'product_id',

        'combo_id'

    ];
}
