<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $table = 'orders';

    protected $fillable = [
        'customer_id',
        'total_price',
        'status',
        'delivery_status'
    ];

    // public $timestamps = true;
    public $incrementing = true;
    public function items(){
        return $this->hasMany(OrderItem::class,'order_id','id');
    }
    public function combos(){
        return $this->hasMany(OrderCombo::class,'order_id','id');
    }
}
