<?php

namespace App\Models;

use App\Http\Traits\Cacheable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory,Cacheable;

    protected $table = 'products';
    protected $fillable = [
        'name',
        'product_type_id',
        'description',
        'price',
        'combo_id',
        'amount',
        'size',
        'image',
        'isDeleted',
        'status'
    ];
    protected $cacheTime = 3600 * 60 * 7;

    public $timestamps = true;

    public $incrementing = true;

    public function productType(){
        return $this->belongsTo(ProductType::class,'product_type_id','id');
    }

    public function listImage(){

        return $this->hasMany(ImageListProduct::class,'product_id','id');
    }
    // public function combo(){
    //     return $this->belongsToMany
    // }

}
