<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderCombo extends Model
{
    use HasFactory;

    protected $table='order_combo';

    protected $fillable = [
        'order_id',
        'combo_id',
        'price',
        'quantity',
        'isDeleted'
    ];
    public $timestamps = true;
    public $incrementing = true;
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

}
