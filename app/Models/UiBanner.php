<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UiBanner extends Model
{
    use HasFactory;
    protected $table = 'ui_banner';

    protected $fillable = [
        'title',
        'description',
        'status'
    ];

    public $timestamps = true;
}
