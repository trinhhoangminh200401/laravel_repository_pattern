<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UiAboutUs extends Model
{
    use HasFactory;
    protected $table = 'ui_about_us';

    protected $fillable = ['title', 'description', 'image'];

    public $timestamps = true;
}
