<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPaymentMethod extends Model
{
    use HasFactory;
    protected $table = 'user_payment_methods';

    protected $fillable = ['customer_id', 'payment_method_id', 'account_details'];

    public $timestamps = true;

    protected $casts = [
        'account_details' => 'array',
    ];
}
