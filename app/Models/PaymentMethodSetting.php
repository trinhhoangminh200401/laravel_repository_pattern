<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentMethodSetting extends Model
{
    use HasFactory;
    protected $table = 'payment_method_settings';

    protected $fillable = ['payment_method_id', 'config'];

    public $timestamps = true;

    protected $casts = [
        'config' => 'array',
    ];
}
