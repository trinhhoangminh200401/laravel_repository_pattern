<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    use HasFactory;
    protected $table = 'deliveries';

    protected $fillable = [
        'order_id',
        'delivery_service',
        'tracking_number',
        'delivery_status'
    ];

    public $timestamps = true;
}
