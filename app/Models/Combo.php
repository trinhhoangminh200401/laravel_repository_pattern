<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Combo extends Model
{
    use HasFactory;

    protected $table = 'combos';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'description',
        'images',
        'isDeleted',
        'status'
    ];

    public $timestamps = true;

    // public function comboProduct ():HasMany
    // {

    //     return $this->hasMany(Product::class,'combo_id','id');
    // }
    public function products()
    {
        return $this->belongsToMany(Product::class, 'combo_detail', 'combo_id', 'product_id');
    }

}
