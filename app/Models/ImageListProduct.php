<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageListProduct extends Model
{
    use HasFactory;

    protected $table= "list_image_products";

    protected $fillable =[
       'attribute',
       'url',
       'isDeleted',
       'product_id'
    ];
    public $timestamps=true;
}
