<?php

namespace App\Models;

use App\Http\Traits\Cacheable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ProductType extends Model
{

    use HasFactory, Cacheable;

    protected $cacheTime = 3600 * 60 *7 ;

    protected $table = 'product_type';

    protected $fillable = ['name', 'image', 'isDeleted'];

    public $timestamps = true;

    public function product(): HasMany
    {
        return $this->hasMany(Product::class, 'product_type_id','id');
    }

}
