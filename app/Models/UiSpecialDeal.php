<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UiSpecialDeal extends Model
{
    use HasFactory;
    protected $table = 'ui_special_deals';

    protected $fillable = [
        'title',
        'description',
        'list_deal_ids',
        'status'
    ];

    public $timestamps = true;
}
