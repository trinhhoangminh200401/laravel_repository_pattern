<?php

namespace App\Core;

use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\Query\Grammars\Grammar;
use Illuminate\Database\Query\Processors\Processor;
use Illuminate\Support\Facades\Cache;

class QueryBuilderWithCache extends QueryBuilder
{

    protected $cacheTime;

    protected $cacheKeys;


    public function __construct(ConnectionInterface $connection, Grammar $grammar = null, Processor $processor = null, int $cacheTime = 0)
    {

        $this->cacheKeys = [
            'product_type' => [
                '' => null
            ],
            'products'=>[
                ''=>null,
                'getProductId'=>null
            ],
            'customers'=>[
                ''
            ]


        ];

        $this->cacheTime = $cacheTime;

        parent::__construct($connection, $grammar, $processor);
    }
    public function shouldCache()
    {
        return request()->has('cache');
    }
    public function cacheKey()
    {
        $tableName = $this->from;
        $action = request()->route()->getActionMethod();

        if ($this->shouldCache() && request()->query('cache') == true) {
            if ($action && $this->cacheKeys[$tableName]) {

                return "{$tableName}.{$action}";
            }
        }
        return null;
    }
    protected function runSelect()
    {
        $cacheKey = $this->cacheKey();

        if ($cacheKey && Cache::store('redis')->has($cacheKey)) {

            return Cache::store('redis')->get($cacheKey);
        }
        if ($this->cacheTime && $this->shouldCache()) {
            return Cache::store('redis')->remember($this->cacheKey(), $this->cacheTime, function () {

                return parent::runSelect();
            });
        }

        return parent::runSelect();
    }
}
