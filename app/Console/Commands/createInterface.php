<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

class createInterface extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:interFace {name} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new interface';

    /**
     * Execute the console command.
     */
    protected  $filesystem;
    public function __construct(FileSystem $filesystem)
    {
        parent::__construct();
        $this->filesystem = $filesystem;
    }
    public function handle()
    {
        $name =  $this->argument('name');
        //
        $suffix = '';
        Str::endsWith($name,  $suffix);
        switch (true) {
            case Str::endsWith($name, 'Repository'):
                $suffix = 'Repository';
                break;
            case Str::endsWith($name, 'Service'):
                $suffix = 'Service';
                break;
            default:
                $suffix = '';
                break;
        }

        switch ($suffix) {
            case 'Repository':
                $folder = 'IRepository';
                break;
            case 'Service':
                $folder = 'IServices';
                break;
            default:
                $folder = '';
                break;
        }

        $directory = app_path("Interface" . ($folder ? "/{$folder}" : ""));
        $path = "{$directory}/{$name}.php";

        $repositoryDirectory=app_path("Repositories");
        $nameReplace = substr($name,1);
        $pathRepository="{$repositoryDirectory}/{$nameReplace}.php";

        // $directory1 = app_path();
        if (!$this->filesystem->isDirectory($directory)) {

            $this->filesystem->makeDirectory($directory, 0755, true);

            $this->info("Created directory: {$directory}");
        }

        // Kiểm tra nếu file đã tồn tại
        if ($this->filesystem->exists($path) || ($this->filesystem->exists($pathRepository))) {
            $this->error("Interface {$name} already exists!");
            return 1;
        }

        // Tạo nội dung của Interface với namespace động
        $content = "<?php\n\nnamespace App\\Interface" . ($folder ? "\\{$folder}" : "") . ";\n\ninterface {$name}\n{\n    // Define your methods here\n}\n";

        $contentRepository= "<?php\n\nnamespace App\\Repositories;"  . "\n\nuse App\\Interface".($folder ? "\\{$folder}" : "")."\\{$name}".";\n\nclass {$nameReplace} implements {$name} \n{\n    // Define your methods here\n}\n";

        // Tạo file Interface
        $this->filesystem->put($path, $content);
         $this->filesystem->put($pathRepository,$contentRepository);

        $this->info("Create Interface success");
        return 0;
    }
}
