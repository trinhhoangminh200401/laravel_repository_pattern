<?php

namespace App\Console\Commands;

use App\Models\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class autoDeleteOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:auto-delete-order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
     $result=Order::where('created_at','<',now()->subDays(2))
            ->where('isDeleted', 1)
            ->update(['isDeleted' => 0]);
        $this->info("Successfully marke  $result orders as deleted.");
     Log::info("cron tab is running");
    }
}
