<?php

namespace App\Http\Middleware;

use App\Mail\MailOpen;
use App\Models\Customer;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use PHPOpenSourceSaver\JWTAuth\Claims\Custom;
use Symfony\Component\HttpFoundation\Response;

class CheckisBlockedAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user = Customer::where('email', $request->email)->first();

        if ($user && $user->isBlocked == 1) {

            Mail::to($user->email)

                ->send(new MailOpen($user->email));

            return response()->json([

                'status' => 403,

                'message' => 'Your Acccount was block !We already send notification to you'

            ], 403);


        }
        // dd($request->getUri());
        // $response=Http::get($request->getUri());
        // dd($response->json());
        return $next($request);
    }
}
