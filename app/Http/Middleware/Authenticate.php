<?php



namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Closure;
class Authenticate
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     */
    public function handle(Request $request,closure $next)
    {
        if (!auth()->check()) {

             return response()->json(['message' => 'Unauthenticated'], 401);

            }
        // Không cần redirect khi dùng JWT
        return $next($request);
    }
    /**
     * Handle an unauthenticated user.
     */
    // public function handleUnauthenticated(Request $request, Response $response)
    // {
    //     // Custom logic when unauthenticated, e.g., logging
    //     // You can also customize the response here
    //     if ($request->expectsJson()) {
    //         return response()->json(['message' => 'Unauthenticated'], 401);
    //     }

    //     return $response;
    // }
}
?>
