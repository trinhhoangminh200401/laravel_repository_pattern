<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next)
    {
        $allowOrigin = ['http://localhost:6969', 'http://localhost:4000', 'http://192.168.2.179:6969'];
        $origin = $request->headers->get('Origin');
        $response = $next($request);
        $response->headers->set('Access-Control-Allow-Credentials', 'true');
        if (in_array($origin, $allowOrigin)) {
            $response->headers->set('Access-Control-Allow-Origin', $origin);
        }
        $response->headers->set('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE,OPTIONS');
        $response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,X-Token-Auth,Authorization');
        $response->headers->set('Access-Control-Allow-Origin', 'http://localhost:6969');
        return $response;
    }
}
