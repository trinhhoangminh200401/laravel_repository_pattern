<?php

namespace App\Http\constant;

class Constant
{
    const PRODUCT = 'api/product';
    const PRODUCT_MANAGE = "api/productMange";
    const PRODUCT_TYPE = 'api/productType';
    const USER = 'api/user';
    const BASE_URL =  'http://food-apps.data/'  ;

}

