<?php

namespace App\Http\constant;

use App\Http\Traits\ApiResponse;
use CloudinaryLabs\CloudinaryLaravel\Facades\Cloudinary;
use http\Env\Request;

class Util
{
    use ApiResponse;
    public static function  getListImageFromString($element)
    {

        $imageString = str_replace("'", '"', $element);



        $imageArray  = json_decode($imageString,true);

        if (json_last_error() !== JSON_ERROR_NONE) {

            return [];
        }

        return count($imageArray) != 0 ? $imageArray : '';
    }
    public static function getModelName($name){

        return str_replace('App\\Models\\','',$name);

    }
    public static function searchModel($model, $keyword)
    {

        return $model->where(function ($query) use ($keyword) {
            $query->where('name', 'LIKE', '%' . $keyword . '%')
                ->orWhere('description', 'LIKE', '%' . $keyword . '%');
        });
    }

    public static function handleUploadImage($request,$nameFolder='uploads')
    {

        $request->validate([

            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:5120',

        ]);
        $file = $request->file('image');
        $uploadedFileUrl = Cloudinary::upload($file->getRealPath(), [
            'folder' => $nameFolder,
            'transformation' => [
                'width' => 1920,
                'height' => 1080,
                'crop' => 'fill'
            ]
        ]);
        return response()->json([
            'message' => 'Image uploaded successfully'.$nameFolder .'!',
            'url' => $uploadedFileUrl->getSecurePath(),
        ], 200);

    }
    public static function utilUploadImage( $request,$nameFolder='uploads')
    {

        $request->validate([

            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:5120',

        ]);
        $file = $request->file('image');

        $uploadedFileUrl = Cloudinary::upload($file->getRealPath(), [
            'folder' => $nameFolder,
            'transformation' => [
                'width' => 1920,
                'height' => 1080,
                'crop' => 'fill'
            ]
        ]);
        return $uploadedFileUrl->getSecurePath();

    }


}
