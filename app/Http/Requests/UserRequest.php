<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {

        switch($this->route()->uri()){

            case "api/login":
                return [

                        'email' => 'required|string|max:255',

                        'password'=>'required'
                    ];
                    break;

            case "api/register":

                return [
                    'name'=> 'required',

                    'email' => 'required|string|max:255',

                    'password' => 'required'
                ];

                break;

            case "api/user/updated/{id}":
                return [
                    'email' => 'unique:customers,email|max:255',

                    'name' => 'string|max:255',

                    'phone_number' => 'string',

                    'delivery_address' => 'string|filled',

                    'image' => 'mimes:jpeg,jpg,png,gif|rmax:10000',

                    'isBlocked' => 'string|number'
                ];
                break;
            default:

              break;

        }


    }
    public function messages()
    {
        return[

            'name.required'=>'Field Name is Required',

            'phone_number.required'=>"Field Number is required",

            'delivery_address.required'=>"Field address is required",

            'image.required'=>'Field Image is required',

            'isBlock.required'=>'Please check isBlocked true or false'

        ];
    }
}
