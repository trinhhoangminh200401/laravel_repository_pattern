<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Traits\ApiResponse;
use App\Interface\IRepository\IAuthRepository;
use App\InterFace\IServices\IEmailService;
use App\InterFace\IServices\ISocialService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    use ApiResponse;
    protected $socialService;
    protected $emailService;
    protected $authRepository;

    public function __construct(IEmailService $emailService, ISocialService $socialService, IAuthRepository $authRepository)
    {
        $this->emailService = $emailService;
        $this->socialService = $socialService;
        $this->authRepository = $authRepository;
        $this->middleware('auth:api', ['except' => ['login', 'register','refreshToken', 'verifyOtpEmail', 'redirectToGoogle', 'handleGoogleCallback','redirectToFacebook','handleFacebookCallback']]);
    }


    public function login(UserRequest $request)
    {
        try {
            $result =  $this->authRepository->authenticate($request->only('email', 'password'));

            if (!$result && !empty($result['status'])) {

                return $this->errorResponse($result['message'], $result['status']);
            }

            return $this->apiResponse($result['data'],$result['status'],$result['message'], $result['status']);

        } catch (\Exception  $err) {

              return $this->errorResponse($err->getMessage(),500);
        }


    }

    public function register(Request $request)
    {
        return $this->emailService->register($request->all());
    }

    public function logout()
    {
        return $this->authRepository->logout();
    }

    public function profile()
    {
        try {

            $result =  $this->authRepository->profile();

            if(!$result){

                return $this->errorResponse('Current user is un valid!',404);

            }
            return  $this->apiResponse($result,200,'success to fetch data',200);

        } catch (\Exception) {
            return $this->errorResponse('Error',500);
        }
    }

    public function refreshToken(Request $request){

        return $this->authRepository->refreshToken($request);
    }


    public function verifyOtpEmail(Request $request)
    {
        return $this->emailService->verifyOtpEmail($request);
    }

    public function redirectToGoogle()
    {

        Log::info('Redirecting to Google Provider');
        return $this->socialService->redirectToProvider('google');
    }

    public function handleGoogleCallback()
    {
        Log::info('Handling Google Provider Callback');
        return $this->socialService->handleProviderCallback('google');
    }
    public function redirectToFacebook(){
        Log::info('Redirecting to Facebook Provider');
        return $this->socialService->redirectToProvider('facebook');

    }
    public function handleFacebookCallback()
    {
        Log::info('Handling Facebook Provider Callback');
        return $this->socialService->handleProviderCallback('facebook');
    }
}

?>
