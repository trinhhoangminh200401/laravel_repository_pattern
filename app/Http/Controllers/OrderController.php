<?php

namespace App\Http\Controllers;

use App\Services\OrderService;
use App\Http\Traits\ApiResponse ;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    use ApiResponse;
    private $orderService;

    public function __construct(OrderService $orderService)
    {

        $this->orderService=$orderService;

    }
    public function createOrder(Request $request){



            $result = $this->orderService->createOrder($request);


            if(!$result){

                return $this->errorResponse('Create fail!',404);

            }
            return $this->apiResponse($result,200,'Success to create',200);
    }

    public function getAllOrder (Request $request){
        try {
            $options = [
                'left_join_order_combo' => true,
                'left_join_order_items'=>true
            ];
            $result = $this->orderService->getOrderList($options,$request);
            if(!$result){
                return $this->errorResponse('Fail to fetch',404);
            }
            return $this->apiResponse($result,200,'Success',200);

        } catch (\Exception $err) {
            return $this->errorResponse($err->getMessage(), 500);


        }

    }
}
