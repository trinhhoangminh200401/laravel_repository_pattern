<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponse;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class ProductController extends Controller
{
    use ApiResponse;

    protected $productService;

    public function __construct(ProductService $productService)
    {


        $this->productService = $productService;
    }

    public function index(Request $request)
    {
        try {

            $filters =
                [
                    'search' => $request->input('search', ''),
                    'status' => $request->input('status', '')
                ];
            $options=[
                'select' => [
                    'products.id',
                    'products.name',
                    'products.description',
                    'products.product_type_id'
                ],
                'with'=>[
                    'listImage' =>function($query){
                        $query->select('product_id','url');
                    },
                    'productType' => function($query){
                        $query->select('id','name');
                    }
                ]

                ];
            $page = $request->input('page');
            $limit = $request->input('limit',1);
            $result = $this->productService->getPaginatedProducts($filters, $limit, $page, $options);
            return $this->apiResponse($result, 200, 'Fetch success', 200);;
        } catch (\Exception $err) {

            return $this->errorResponse($err->getMessage(), 500);
        }
    }
    public function topProduct(Request $request)
    {
        try {

            $filters =
                [
                    'search' => $request->input('search', ''),
                    'status' => $request->input('status', '')
                ];
            $options=[
                'with'=>[
                    'listImage'=>false
                ]

            ];
            $page = $request->input('page');

            $limit = $request->input('limit');
            $result = $this->productService->getTopProducts($filters, $limit, $page, $options);
            return $this->apiResponse($result, 200, 'Fetch success', 200);;
        } catch (\Exception $err) {

            return $this->errorResponse($err->getMessage(), 500);
        }
    }

    public function insertProduct(Request $request)
    {

        try {

            $food = $this->productService->insertProduct($request);

            return response()->json(

                [
                    'response' => $food,

                ],
                200
            );
        } catch (\Exception $e) {

            return response()->json([

                'error' => 'cant not add record',

                'message' => 'you missing record to add'

            ], 500);
        }
    }


    // public function getPageData(Request $request)
    // {

    //     try {

    //         $currentPage = $request->input('currentPage', 1);

    //         $perPage = $request->input('perPage', 10);

    //         $param = $request->input('search', '');

    //         $data = $this->productService->getProductPage($currentPage, $perPage, $param);
    //         if(!$data){
    //             return $this->errorResponse('Not Product in list',404);
    //         }
    //         return $this->apiResponse($data,'getPageSuccess',200);
    //     }
    //     catch (\Exception $err) {
    //         return $this->errorResponse($err->getMessage(), 500);

    //     }

    // }
    public function getProductId($id)
    {


        return $this->productService->getProductId($id);
    }
    // public function searchProduct($param = '')
    // {

    //     return $this->productService->searchProduct($param);
    // }

    // public function getAllListProduct(Request $request)
    // {

    //     try {


    //         $input = $request->all();

    //         $options = [

    //             "get" => true,

    //             "select" => [
    //                 '*'

    //             ]
    //         ];
    //         $result = $this->productService->getList($input, $options);

    //         return $this->apiResponse($result,200,'success',200);


    //     } catch (\Exception $err) {


    //     }
    // }

    public function multipleDestroyedProduct(Request $request)
    {

        try {

            $data = $this->productService->mulipleDeleteProduct($request);

            if (!$data) {

                return $this->errorResponse('product not found', 404);
            }
            return $this->apiResponse($data, 'update success', 200);
        } catch (\Exception $e) {

            return $this->errorResponse($e->getMessage(), 500);
        }
    }
    public function updateProduct($id, Request $request)
    {

        return $this->productService->updateProduct($request, $id);
    }

    public function destroyProductId($id)
    {

        return $this->productService->destroyProduct($id);
    }
    public function deleteProductId($id)
    {

        return $this->productService->deleteProduct($id);
    }
}
