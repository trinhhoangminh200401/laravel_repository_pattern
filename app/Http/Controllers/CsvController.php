<?php

namespace App\Http\Controllers;

use App\Http\constant\Constant;
use App\Http\constant\Constantname;
use App\InterFace\IServices\ICsvService;
use App\Models\Product;
use App\Models\ProductType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

use Symfony\Component\HttpFoundation\StreamedResponse;

class CsvController extends Controller
{
    private $csvService;
    public function __construct(ICsvService $csvService)
    {
        $this->csvService=$csvService;
    }

    public function exportFileProductCsv (){

        $url = Constant::BASE_URL.''.Constant::PRODUCT;
        $response = Http::get($url);

        $data = $response->json();
        dd($data);
        // dd($data);
        $headers = ['name', "product_type_id",'description', 'price', 'amount', 'size',"image","isDeleted","status","created_at","updated_at"];
        $fileName = 'products.csv';

        return $this->csvService->exportFileCsv($data, $headers, $fileName);
    }
    public function importProductCsv(Request $request)
    {
        $request->validate([
            'importCsvFile' => 'required|mimes:csv,xlsx',
        ]);

        try {
            $this->csvService->importFileCsv(['file' => $request->file('importCsvFile')], Product::class);

            return redirect()->route("csv")->with('success', 'Product CSV Imported Successfully');
        }
        catch (\Exception $th) {
            return redirect()->route("csv")->with('error', 'Error importing Product CSV');
        }
    }
    public function importTypeCsv(Request $request){

        $request->validate([
            'importCsvTypeFile' => 'required|mimes:csv,xlsx',
        ]);

        try {
            $this->csvService->importFileCsv(['file' => $request->file('importCsvTypeFile')], ProductType::class);

            return redirect()->route("csv")->with('success', 'Product CSV Imported Successfully');
        } catch (\Exception $th) {
            return redirect()->route("csv")->with('error', 'Error importing Product CSV');
        }


    }
    public function exportFileTypeCsv(){
        $response = Http::get(Constant::BASE_URL . Constant::PRODUCT_TYPE);
        $data = $response->json();

        $headers = ['name', 'image'];
        $fileName = 'type.csv';

        return $this->csvService->exportFileCsv($data, $headers, $fileName);
    }
    // public function exportFileCsv()
    // {

    //     try {1

    //         $foods = Food::all();

    //         $csvFileName = 'food.csv';

    //         $headers = [
    //             'Content-Type' => 'text/csv',
    //             'Content-Disposition' => 'attachment; filename="' . $csvFileName . '"',
    //         ];
    //         $handle = fopen('php://output', 'w');
    //         fputcsv($handle, ['name', 'description', 'price']);
    //         foreach ($foods as $food) {
    //             fputcsv($handle, [$food->name, $food->description, $food->price]);
    //         }

    //         fclose($handle);


    //       return  Response::make('', 200, $headers);

    //     } catch (\Exception $err) {

    //         redirect()->back();

    //     }
    // }
    // public function exportFileCsv()
    // {

    //     $response = Http::get('http://food-apps.data/api/product');
    //     $data = $response->json();

    //     $response = new StreamedResponse(function () use ($data) {

    //         $handle = fopen('php://output', 'w');

    //         fputcsv($handle, ['name', 'description', 'price','quality','size']);

    //         foreach ($data as $row) {
    //             $name = isset($row['name']) && !empty($row['name']) ? $row['name'] : 'null';
    //             $description = isset($row['description']) && !empty($row['description']) ? $row['description'] : 'null';
    //             $price = isset($row['price']) && !empty($row['price']) ? $row['price'] : 'null';
    //             $quality = isset($row['quality']) && !empty($row['quality']) ? $row['quality'] : 'null';
    //             $size = isset($row['size']) && !empty($row['size']) ? $row['size'] : 'null';

    //             fputcsv($handle, [$name, $description, $price, $quality, $size]);
    //         }

    //          fclose($handle);
    //     });
    //     $response->headers->set('Content-Type', 'text/csv');
    //     $response->headers->set('Content-Disposition', 'attachment; filename='.Constantname::nameFile['product'].'.csv');

    //     return $response;
    // }
    // public function importFileCsv(Request $request)
    // {

    //     // $file = $request->file('importCsvFile');

    //     // $filePath = $file->getRealPath();

    //     // dd($filePath);
    //     $request->validate([

    //         'importCsvFile' => 'required|mimes:csv,xlsx',

    //     ]);
    //     try {


    //         $file = $request->file('importCsvFile');

    //         $filePath = $file->getRealPath();

    //         $fileOpen = fopen($filePath, 'r+');

    //         $header= fgetcsv($fileOpen);



    //         $escapedHeader = [];

    //         foreach ($header as $key => $value) {
    //             $lheader = strtolower($value);
    //             $escapedItem = preg_replace('/[^a-z]/', '', $lheader);

    //             array_push($escapedHeader, $escapedItem);
    //         }

    //         DB::beginTransaction();

    //         while($columns = fgetcsv($fileOpen)){
    //             if (count($columns) == count($escapedHeader)){
    //                 $data = array_combine($escapedHeader, $columns);
    //                 Product::updateOrCreate(
    //                     ['name' => $data['name']],
    //                     $data
    //                 );
    //             }
    //             else{

    //                 return redirect()->route("csv")->with('error'," Your's csv are not true  please check structure");
    //                 break;
    //             }

    //         }

    //         DB::commit();


    //     } catch (\Exception $th) {

    //         DB::rollBack();

    //         Log::info('show error', ['error' => $th->getMessage()]);
    //     }
    // }
}
