<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponse;
use App\Services\ProductTypeService;
use Illuminate\Http\Request;

class ProductTypeController extends Controller
{


    private $productTypeService;

    use ApiResponse;

    public function __construct(ProductTypeService $productTypeService)
    {
        return $this->productTypeService = $productTypeService;
    }
    // public function getProductBelongType(Request $request)
    // {

    //     $id = $request->query('type');

    //     $page = $request->query('page');

    //     $search = $request->input('search');

    //     $perPage = $request->input('perPage');

    //     return  $this->productTypeService->getListBelongTypes($id, $page, $perPage,$search);


    // }

    public function getTypeId(Request $request)
    {

        try {
            $result = $this->productTypeService->getTypeById($request->input('id'));


            return $this->apiResponse($result, 200, 'Get Type id success', 200);
        } catch (\Exception $err) {

            return $this->errorResponse($err->getMessage(), 500);
        }
    }
    public function getProductsByType(Request $request)
    {

        try {

            $id = $request->input('id');

            $currentPage = $request->input('page', 1);

            $perPage = $request->input('limit', 10);


            $filters =
            [
               'status'=> $request->input('status','actived'),
               'search' => $request->input('search', ''),
               'sort_by'=>[
                    $request->query('sort_by')=>"asc"
               ]
            ];


            $result = $this->productTypeService->getBelongTypeIdProduct($id, $currentPage, $perPage, $filters);

            if (empty($id)) {

                return $this->errorResponse('error', 404);
            }
            if (!$result) {
                return $this->errorResponse('Cant not find ProductType!', 404);

            }
            return $this->apiResponse($result, 200, 'Success', 200);

        } catch (\Exception $err) {

            return $this->errorResponse($err->getMessage(), 500);
        }
    }

    public function getProductType(Request $request)
    {
        try {

            $filters = [
                'name' => $request->input('name')
            ];
            $limit = $request->input('limit', 1);
            $page = $request->input('page', 1);
            $options = [
                "select" => [
                    'product_type.id',
                    'product_type.name',
                    'product_type.image',
                    'product_type.created_at'
                ]
            ];
            $result = $this->productTypeService->getAllType($filters,$limit, $page,$options);
            if (!$result) {
                return $this->errorResponse('Fail to fetch',404);
            }
            return $this->apiResponse($result,200,'Success to fetch',200);
        } catch (\Exception $err) {

            return $this->errorResponse($err->getMessage(), 500);
        }
    }
    public function createProductType(Request $request)
    {
        try {

            $result=$this->productTypeService->insertProductType($request->all());

            if(!$result){

                return  $this->errorResponse('Fail to create',404);

            }
            return $this->apiResponse($result,200,'success',200);
        } catch (\Exception $err) {

            return $this->errorResponse($err->getMessage(),500);
        }

    }
    public function mutipleDeleteProductType(Request $request)
    {
        try {
            $result = $this->productTypeService->multipleDestroyProductType($request);
            if(!$result){
                return $this->errorResponse('Fail to deleted', 404);
            }
            return $this->apiResponse($result,200,'success to deleted!',200);

        } catch (\Exception $err) {

             return $this->errorResponse($err->getMessage(),500);
        }

    }
}
