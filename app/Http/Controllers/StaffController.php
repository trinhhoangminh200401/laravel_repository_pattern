<?php

namespace App\Http\Controllers;

use App\Http\constant\Util;
use App\Http\Traits\ApiResponse;
use App\Models\Staff;
use App\Services\StaffService;
use Illuminate\Http\Request;

class StaffController extends Controller
{

    use ApiResponse;

    protected StaffService $staffService;

    public function __construct(StaffService $staffService)
    {
        $this->staffService = $staffService;
    }

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            // Lấy các tham số tìm kiếm và lọc từ request
            $filters = [
                'status' => $request->input('status'),
                'search' => $request->input('search')
            ];

            // Lấy tham số phân trang từ request (nếu không có thì dùng giá trị mặc định)
            $limit = $request->input('limit', 10);  // Số bản ghi mỗi trang
            $page = $request->input('page', 1);     // Trang hiện tại

            // Gọi service với các tham số tìm kiếm và phân trang
            $staff = $this->staffService->getAllStaff($filters, $limit, $page);

            return $this->apiResponse($staff, 200, 'Staff retrieved successfully.');
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(), 500);
        }
    }


    public function show($id): \Illuminate\Http\JsonResponse
    {
        try {
            $staff = $this->staffService->getStaffById($id);
            if (!$staff) {
                return $this->errorResponse('Staff not found', 404);
            }
            return $this->apiResponse($staff, 200, 'Staff details retrieved successfully.');
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(), 500);
        }
    }
// --------------- this is my function --------------

    public function getStaffIds(Request $request): \Illuminate\Http\JsonResponse{

        try {

            $currentStaff = $this->staffService->getIdFromStaff($request->input('id'));

            return $this->apiResponse($currentStaff,200,'Get Current Staff Success',200);

        } catch (\Exception $err) {
            //throw $th;
            return $this->errorResponse($err->getMessage(),500);
        }

    }


    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = $request->all();
        try {
            // Validate only if the image field is present
//            if ($request->hasFile('image')) {
//                $result = Util::utilUploadImage( $request, 'staffs');
//                $data['image'] = $result; // Assuming handleUploadImage returns the path or URL of the uploaded image
//            }
            $staff = $this->staffService->createStaff($data);
            return $this->apiResponse($staff, 200, 'Staff created successfully.', 201);
        } catch (\Exception $e) {
            echo $e->getMessage();
            return $this->errorResponse($e->getMessage(), 500);
        }
    }

    public function update(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        try {
            // Call the updateStaff method from the service with request data
            $result = $this->staffService->updateStaff($request, $id);

            // If update failed (e.g., staff not found or validation error)
            if (!$result['status']) {
                return $this->errorResponse($result['message'], 400); // Use 400 for client errors like validation
            }

            // On success, return the updated staff data
            return $this->apiResponse($result['data'], 200, $result['message']);

        } catch (\Exception $e) {
            // Handle unexpected exceptions
            return $this->errorResponse('An error occurred while updating the staff.', 500);
        }
    }

    public function destroy($id): \Illuminate\Http\JsonResponse
    {
        try {
            $deleted = $this->staffService->deleteStaff($id);
            if (!$deleted) {
                return $this->errorResponse('Staff not found', 404);
            }
            return $this->apiResponse(null, 200, 'Staff deleted successfully.');
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(), 500);
        }
    }
}
