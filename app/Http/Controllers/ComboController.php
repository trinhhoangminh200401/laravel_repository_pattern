<?php

namespace App\Http\Controllers;

use App\Models\ComboProduct;
use App\Services\ComboService;
use Illuminate\Http\Request;

class ComboController extends Controller
{
    private  $comboService;
    public function  __construct(ComboService $comboService)
    {
        $this->comboService = $comboService;
    }

    public function index(Request $request)
    {
        $filters=[
            'search' => $request->input('search',''),
            'status' => $request->input('status','actived')
        ];
        $limit = $request->input('limit',10);
        $page= $request->input('page',1);
        $options = [
            'select'=>[
                'combos.name',
                'combos.description',
                'combos.created_at',
                'combos.images'
            ]

        ];
        return $this->comboService->getAllCombo($filters,$limit,$page,$options);
    }

    public function store(Request $request)
    {
        $inputs=[
            'name'=>$request->input('name'),
            'description'=>$request->input('description'),
             'product_ids'=>$request->input('product_ids',[])
        ];
        return $this->comboService->insertComboProduct($inputs);
    }

    public function getComboId(Request $request)
    {

        $id = $request->query('id');

        return $this->comboService->getComboId($id);
    }

    // public function destroyComboProduct(ComboProduct $comboProduct) {

    //     return $this->comboService->destroyCombo($comboProduct->id);


    // }

}
