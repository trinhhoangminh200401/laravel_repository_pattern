<?php

namespace App\Http\Controllers;

use App\Http\constant\Util;
use App\Http\Requests\UserRequest;
use App\Http\Traits\ApiResponse;
use App\Services\UserService;
use CloudinaryLabs\CloudinaryLaravel\Facades\Cloudinary;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;


class UserController extends Controller
{
    use ApiResponse;

    protected $userService;

    public function __construct(UserService $userService)
    {

        $this->userService = $userService;
    }

    public function  getUserId(Request $request)
    {
        try {

            $data = $this->userService->getUserId($request);

            return $this->apiResponse($data, 'Get user Id success!', 200);
        } catch (\Exception $err) {

            return $this->errorResponse(500, $err->getMessage());
        }
    }


    public function getAllUser(Request $request)
    {
        try {

            $filters = [

                'search'=>$request->input('search'),

                'status'=>$request->input('status')

            ];

            $options=[
                'select'=>[
                    'customers.id',
                    'customers.name',
                    'customers.phone_number',
                    'customers.delivery_address',
                    'customers.image',
                    'customers.created_at'
                ],

            ];

            $page = $request->input('page',1);

            $limit = $request->input('limit',1);

            $results = $this->userService->getAllUsers($filters, $limit, $page,$options);

            return $this->apiResponse($results,200,'Get Data success',200);
        } catch (\Exception $err) {

            return $this->errorResponse($err->getMessage(), 500);
        }
    }

    public function blockCurrentUser(Request $request)
    {

        return $this->userService->blockUser($request->id);
    }
    public function blockMultipleUser(Request $request)
    {

        return $this->userService->blockMultiple($request);
    }
    public function updateUserData(UserRequest $request, $id)
    {


        try {

            $result = $this->userService->updateUser($request->validated(), $id);
            if (!$result['status']) {
                return $this->errorResponse($result['message'], $result['status_code']);
            }
            return $this->apiResponse($result['data'], $result['status_code'], "Update user success", $result['status_code']);

        } catch (\Illuminate\Validation\ValidationException $err) {
            return $this->errorResponse(500, $err->errors());
        }
    }

    // public function uploadUser(Request $request)
    // {
    //     $request->validate([
    //         'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:5120',
    //     ]);

    //     $result = Util::handleUploadImage($request, 'users');

    //     return  $result;

    // }
    // public function uploadProduct(Request $request)
    // {
    //     $request->validate([
    //         'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:5120',
    //     ]);

    //     $result = Util::handleUploadImage($request, 'products');

    //     return  $result;


    // }

}
