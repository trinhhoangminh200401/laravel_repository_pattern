<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponse;
use App\Interface\IRepository\IAuthRepository;
use App\InterFace\IServices\IEmailService;
use App\InterFace\IServices\ISocialService;
use App\Repositories\AuthRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AuthAdminController extends Controller
{
    use ApiResponse;

    protected $authRepository;
    public function __construct(IAuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;
        $this->middleware('auth:api-admin,staff', ['except' => ['login','profile', 'logout', 'refreshToken']]);
    }

    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        $credentials = $request->only('email', 'password');


        return $this->authRepository->authenticate($credentials);
    }
    public function profile()
    {
        return $this->authRepository->profile();
    }
    public function logout(): \Illuminate\Http\JsonResponse
    {
        Auth::guard('api-admin')->logout();

        return $this->apiResponse([], true, 'Successfully logged out');
    }

    public function refreshToken(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->authRepository->refreshToken($request->input('refreshToken'));
    }


}
