<?php

namespace App\Http\Controllers;

use App\Http\constant\Util;
use App\Http\Traits\ApiResponse;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    use ApiResponse;
    public function uploadController(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $data= Util::utilUploadImage($request, $request->query('name'));
            return $this->apiResponse($data, 200, 'Image uploaded successfully');
        }
        catch (\Exception $e) {
                return $this->errorResponse($e->getMessage(), 500);
            }
    }
}
