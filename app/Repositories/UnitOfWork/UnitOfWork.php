<?php
namespace App\Repositories\UnitOfWork;

use App\Interface\IRepository\IOrderRepository;
use App\Interface\IUnitOfWork\IUnitOfWork;
use Illuminate\Support\Facades\DB;

class UnitOfWork implements IUnitOfWork{

    private $repositories = [];

  
    public function beginTransaction()
    {
         DB::beginTransaction();
    }
    public function commit()
    {
        DB::commit();
    }
    public function rollback()
    {
        DB::rollback();
    }

    public function getRepository(string $repositoryClass)
    {
        if (!isset($this->repositories[$repositoryClass])) {
            $this->repositories[$repositoryClass] = app($repositoryClass);
        }

        return $this->repositories[$repositoryClass];
    }
}
?>
