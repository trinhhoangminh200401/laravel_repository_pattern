<?php

namespace App\Repositories\Repository;

use App\Interface\IRepository\IOrderItemRepository;
use App\Models\OrderItem;

class OrderItemRepository extends BaseRepository implements IOrderItemRepository
{
    public function getModel(): string
    {
        return OrderItem::class;
    }
    // Define your methods here
}
