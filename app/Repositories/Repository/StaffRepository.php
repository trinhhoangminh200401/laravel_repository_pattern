<?php

namespace App\Repositories\Repository;

use App\Interface\IRepository\IStaffRepository;
use App\Models\Staff;

class StaffRepository extends BaseRepository implements IStaffRepository
{
    public function getModel(): string
    {
        return Staff::class;
    }
    public function findByEmail($email)
    {

        return $this->model->where('email', '=', $email)->first();
    }
    public function findById($id, $conditions = [])
    {
        // Query the staff member by ID and check if isDeleted is false
        return Staff::where('id', $id)
            ->where($conditions) // Apply the additional conditions (e.g., isDeleted = false)
            ->first(); // Retrieve the first matching record
    }

    // Get all staff members
    public function getAllStaff($filters = [], $limit = 10, $page = 1)
    {
        $query = $this->model->newQuery();

        // Filter theo status
        if (isset($filters['status'])) {
            $query->where('isBlocked', $filters['status']);
        }

        // Tìm kiếm theo tên, email, số điện thoại
        if (!empty($filters['search'])) {
            $search = $filters['search'];
            $query->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', "%{$search}%")
                    ->orWhere('email', 'LIKE', "%{$search}%")
                    ->orWhere('phone_number', 'LIKE', "%{$search}%");
            });
        }
        $query->where('isDeleted', '0');
        // Tính toán phân trang
        $totalRecords = $query->count();
        $offset = ($page - 1) * $limit;

        // Lấy dữ liệu với limit và offset
        $staffData = $query->offset($offset)->limit($limit)->get();

        // Tính toán thông tin phân trang
        $totalPages = ceil($totalRecords / $limit);
        $nextPage = ($page < $totalPages) ? $page + 1 : null;
        $prevPage = ($page > 1) ? $page - 1 : null;

        // Trả về kết quả cùng với các thông tin phân trang
        return [
            'data' => $staffData,
            'pagination' => [
                'totalRecords' => $totalRecords,
                'totalPages' => $totalPages,
                'currentPage' => $page,
                'perPage' => $limit,
                'nextPage' => $nextPage,
                'prevPage' => $prevPage,
            ]
        ];
    }


    public function blockUser($id): bool
    {
        $currentStaff = $this->model->find($id);

        if (!$currentStaff) {

            return false;
        }
        if ($currentStaff && $currentStaff->isDeleted != 1) {

            $currentStaff->isBlocked = 1;

            $currentStaff->updated_at = now();

            $currentStaff->save();

            return true;
        }

        return false;
    }
    public function  blockMultiple($request): bool
    {
        $this->model->whereIn('id', $request)
            ->where('isDeleted', '!=', 1)
            ->update([
                'isBlocked' => 1,
                'updated_at' => now()
            ]);
        return true;
    }
}
