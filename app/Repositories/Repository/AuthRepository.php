<?php

namespace App\Repositories\Repository;

use App\Interface\IRepository\IAuthRepository;
use App\Mail\OtpMail;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;
use PHPOpenSourceSaver\JWTAuth\JWTAuth as JWTAuthJWTAuth;

class AuthRepository implements IAuthRepository
{
    public function Authenticate($credentials = [])
    {
        Log::info('Credentials Received', $credentials);

        if (!$token = JWTAuth::attempt($credentials)) {

            return   [
                'status' => 403,
                'message' => 'UnAuthenticated',
            ];

        }

        $user = Auth::user();

        if ($user->isVerified == 0) {

            Auth::logout();

            Cache::clear();

            return [

                'status ' => 403,

                "message" => 'Your account wasnt verified'
            ];
        }
        // if($user->isBlock == 0)
        // {

        //     Auth::logout();

        //     Cache::clear();

        //     return response()->json([

        //         'status '=>403,

        //         "message"=>'Your account was block'
        //     ]);
        // }

        // $cachedToken = Cache::get('user_token_' . $user->id);

        // if ($cachedToken) {
        //     JWTAuth::setToken($cachedToken)->invalidate();
        // }

        Cache::put('user_token_' . $user->id, $token);

        $refresh = $this->createRefreshToken();

        return $this->respondWithToken($token, $refresh);
    }

    public function profile()
    {
        return Auth::user();


    }
    public function logout()
    {

        Cache::forget('user_token_' .Auth::user()->id);
        Auth::logout();

        return response()->json(['status' => 'success', 'message' => 'Successfully logged out']);
    }
    public function createRefreshToken()
    {
        $data = [

            'sub' => Auth::user()->id,

            'random' => rand() . time(),

            'exp' => time() + config('jwt.refresh_ttl')
        ];
        return JWTAuth::getJWTProvider()->encode($data);
    }
    public function refreshToken($request)
    {

        try {

            $newRefreshToken = $request->input('refreshToken');


            $decode = JWTAuth::getJWTProvider()->decode($newRefreshToken);

            if (!isset($decode['sub'])) {

                return response()->json(['error' => 'Invalid token: Missing user identifier'], 400);

            }
            $user = Customer::find($decode['sub']);


            if (!$user) {
                return response()->json(['error' => 'User not found'], 404);
            }

            $currentToken  = Cache::get('user_token_' . $user->id);


            if ($currentToken) {

                JWTAuth::setToken(Cache::get('user_token_' .  $user->id))->invalidate(true);
            }

            $token = Auth::login($user);

            Cache::put('user_token_' . Auth::user()->id, $token);

            $newRefreshToken = $this->createRefreshToken();

            return $this->respondWithToken($token, $newRefreshToken);
        } catch (\Throwable $th) {

            Log::error('Failed to refresh token: ' . $th->getMessage());

            return response()->json(['error' => 'Failed to refresh token: ' . $th->getMessage()], 500);
        }
    }

    protected function respondWithToken($token, $refresh)
    {
        return
                [

                    'status' => 200,

                    'data' => [

                        'access_token' => $token,

                        'refresh_token' => $refresh,

                        'token_type' => 'bearer',

                        'expires_in' => Auth::factory()->getTTL() * 60 * 24 * 7,
                    ],
                    'message'=>'Login successful'
                ];
    }


}
