<?php

namespace App\Repositories\Repository;

use App\Interface\IRepository\IAuthRepository;
use App\Models\Staff;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;


class AuthAdminRepository implements IAuthRepository
{

    protected function respondWithToken($token, $refresh)
    {
        return response()->json(
            [

                'status' => 200,

                'data' => [

                    'access_token' => $token,

                    'refresh_token' => $refresh,

                    'token_type' => 'bearer',

                    'expires_in' => Auth::factory()->getTTL() * 60 * 24 * 7,
                ],
                'message' => 'Login successful'
            ],
            200
        );
    }
    public function Authenticate($credential = []): \Illuminate\Http\JsonResponse
    {
        if (!$token = Auth::guard('api-admin')->attempt($credential)) {
            return response()->json(['status' => 'error', 'message' => 'Unauthorized'], 401);
        }

        $user = Auth::guard('api-admin')->user();

        if ($user->isBlocked == 1) {
            Auth::guard('api-admin')->logout();
            return response()->json(['status' => 403, 'message' => 'Your account is blocked']);
        }

        Cache::put('admin_token_' . $user->id, $token);

        $refresh = $this->createAdminRefreshToken();
        return $this->respondWithToken($token, $refresh);
    }

    public function logout() {

        Auth::guard('api-admin')->logout();

        Cache::forget('admin_token_'. Auth::guard('api-admin')->user()->id);

        return response()->json(['status' => 'success', 'message' => 'Successfully logged out']);

    }

    public function createAdminRefreshToken()
    {
        $data = [
            'sub' => Auth::guard('api-admin')->user()->id,
            'random' => rand() . time(),
            'exp' => time() + config('jwt.refresh_ttl'),
        ];
        return JWTAuth::getJWTProvider()->encode($data);
    }
    public function refreshToken($request)
    {

        try {

            $newRefreshToken = $request;


            $decode = JWTAuth::getJWTProvider()->decode($newRefreshToken);

            if (!isset($decode['sub'])) {

                return response()->json(['error' => 'Invalid token: Missing user identifier'], 400);
            }
            $user = Staff::find($decode['sub']);



            if (!$user) {
                return response()->json(['error' => 'User not found'], 404);
            }

            $currentToken  = Cache::get('admin_token_' . $user->id);


            if ($currentToken) {


                JWTAuth::setToken(Cache::get('admin_token_' .  $user->id))->invalidate();
            }

            $token = Auth::guard('api-admin')->login($user);



            Cache::put('admin_token_' . Auth::guard('api-admin')->user()->id, $token);

            $newRefreshToken = $this->createAdminRefreshToken();

            return $this->respondWithToken($token, $newRefreshToken);
        } catch (\Throwable $th) {

            Log::error('Failed to refresh token: ' . $th->getMessage());

            return response()->json(['error' => 'Failed to refresh token: ' . $th->getMessage()], 500);
        }
    }

    public function profile(): \Illuminate\Http\JsonResponse
    {
        return response()->json(
            [
                'data' => Auth::guard('api-admin')->user(),
                'message' => 'success',
                'status' => 200

            ],
            200

        );
    }
}

?>



