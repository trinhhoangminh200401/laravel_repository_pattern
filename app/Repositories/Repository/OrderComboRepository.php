<?php

namespace App\Repositories\Repository;

use App\Interface\IRepository\IOrderComboRepository;
use App\Models\OrderCombo;

class OrderComboRepository extends BaseRepository implements IOrderComboRepository
{
    // Define your methods here
    public function getModel(): string
    {
        return OrderCombo::class;
    }


}
