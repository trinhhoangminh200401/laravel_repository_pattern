<?php

namespace App\Repositories\Repository;

use App\Interface\IRepository\IBaseRepository ;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository implements IBaseRepository
{
    protected $model;


    public function __construct()
    {

        $this->setModel();

    }

    abstract public function getModel() : string;


    public function setModel():void
    {

        $this->model = app()->make(

            $this->getModel()

        );

    }

    public function getAll(){

          return  $this->model->where('isDeleted','0');

    }

    public function find($id):?Model{

        return $this->model->find($id);
    }

    public function getSize():Collection{


        $chunkSize = 100;

        $results = collect();

        $this->model->chunk($chunkSize, function ($items) use ($results) {

            foreach ($items as $item) {

                $results->push($item);

            }
        });

        return $results;

    }

    public function create($attributes = []): Model
    {

        $result = $this->model->create($attributes);

        return $result ?? false;
    }

    public function update($id, $attributes = [])
    {
        $result = $this->model->find($id);
        if ($result) {

            $result->update($attributes);

            return $result;
        }

        return false;
    }

    public function destroy($id): bool
    {

        $result = $this->model->find($id);

        if ($result && $result->isDeleted==0) {

            $result->isDeleted=1;

            $result->save();

            return true;
        }
        else if ($result->isDeleted == 1){

            return false;

        }

        return false;
    }
    public function multipleDestroy($data = [])
    {
        $result = $this->model->whereIn('id',$data)->update([

                'isDeleted' => 1,

                'updated_at' => now()
        ]);
         return $result ?? false ;
    }
    public function deleted($id):bool
    {

        $result = $this->model->find($id);

        if($result){

            $result->delete();

            return true;

        }
        return false;
    }

    public function paginationData ($perPage,$pageSize , $relation = null,$query=null)
    {

        $query = $relation ?: $query;

        return $query->offset($perPage)->limit($pageSize)->where('isDeleted','0')->get();

        // return $this->model->offset($perPage)->limit($pageSize)->where('isDeleted', '0')->get();
    }

    // public function searchOptions($query, $options = [])
    // {
    //     if(!empty($options['select'])){
    //         $query= $query->select($options['select']);
    //     }

	// 	if(!empty($options['sort'])) {
	// 		foreach($options['sort'] as $key => $value) {
	// 			$type = str_replace('sorting_', '', $value);
	// 			$query = $query->orderBy($key, $type);
	// 			if(!empty($options['second_sort'])) {
	// 				foreach($options['second_sort'] as $key => $value) {
	// 					$query = $query->orderBy($key, $type);
	// 				}
	// 			}
	// 		}
	// 	}
    //     if (!empty($options['with'])) {
    //         foreach ($options['with'] as $key => $function) {
    //             $query = $query->with([$key => $function]);
    //         }
    //     }

    //     if (!empty($options['get'])) {
    //         $query = $query->get();
    //     }
    //     return $query;
    // }
}
