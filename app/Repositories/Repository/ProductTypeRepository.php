<?php

namespace App\Repositories\Repository;

use App\Http\constant\Util;
use App\Interface\IRepository\IProductTypeRepository;
use App\Models\ProductType;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class ProductTypeRepository extends BaseRepository implements IProductTypeRepository
{
    public function getModel(): string
    {
        return ProductType::class;
    }
    public function getAllType($filters = [],$limit = '', $page = '', $options = [])
    {
        $query= $this->getAll();
        if(!empty($options['select'])){
            $query->select($options['select']);
        }
        if(!empty($filters['search'])){
            $search = $filters['search'];

            $query->where(function (Builder $subQuery) use ($search) {

                $subQuery->whereRaw('REPLACE(LOWER(name), \' \', \'\') LIKE LOWER(?)', ["%{$search}%"]);
            });
        }
        $offset = ($page - 1) * $limit == 0 ? 1 :($page - 1) * $limit ;

        $totalPages =  $query->count() / ($limit ?? 1);

        $nextPage = ($page < $totalPages) ? $page + 1 : null;

        $prevPage = ($page > 1) ? $page - 1 : null;

        $productType= $this->paginationData($offset, $limit, null, $query);


        return [
            'data' => $productType,

            'pagination' => [

                'totalRecords' => $productType->count(),

                'totalPages' => $totalPages ?? null,

                'currentPage' => $page,

                'perPage' => $limit,

                'nextPage' =>  $nextPage,

                'prevPage' => $prevPage
            ]

        ];

    }
    public function getListBelongType($id)
    {
        return $this->model->where("id", $id)->where('isDeleted',0)->with('product')->first();
    }
    public function getProductListBelongToType($id, $page = '', $limit = 10, $filters = [])
    {

        $productType =  $this->getListBelongType($id);

        if (!$productType) {

            return false;
        }
        $query = $productType->product();

        if (!empty(trim($filters['search']))) {
            $search = $filters['search'];

            $query->where(function (Builder $subQuery) use ($search) {

                $subQuery->whereRaw('REPLACE(LOWER(name), \' \', \'\') LIKE LOWER(?)', ["%{$search}%"])

                    ->orWhereRaw('REPLACE(LOWER(description), \' \', \'\') LIKE LOWER(?)', ["%{$search}%"]);
            });
        }

        if (!empty($filters)) {

            if (isset($filters['status'])) {

                $query->where('status', $filters['status']);
            }

            if (isset($filters['sort_by'])) {

                if (!empty(key($filters['sort_by']) == 'id')) {

                    $query->orderBy('id', $filters['sort_by']['id'] == 'asc' ? 'asc' : 'desc');

                }

            }
        }

        $offset = ($page - 1) * $limit;


        $query = $query->with('listImage');




        // foreach ($paginatedProducts as $product) {


        //     $product->image = is_array($product->image) ?  Util::getListImageFromString($product->image) : $product->image;
        // }

        $totalProducts = $query->count();

        $totalPages = ceil($totalProducts / $limit);

        $offset = ($page - 1) * $limit;

        $paginatedProducts = $this->paginationData($offset, $limit, null, $query);

        $nextPage = ($page < $totalPages) ? $page + 1 : null;

        $prevPage = ($page > 1) ? $page - 1 : null;

        return [

            'id' => $productType->id,

            'name' => $productType->name,

            'image' => $productType->image,

            'products' => $paginatedProducts,

            'pagination' => [

                'totalRecords' =>  $totalProducts,

                'totalPages' => $totalPages ?? null,

                'currentPage' => $page,

                'perPage' => $limit,

                'nextPage' =>  $nextPage,

                'prevPage' => $prevPage
            ]
        ];
        // DB::enableQueryLog();

        // if (!empty($search)) {

        //     $data->whereHas('product', function (Builder $query) use ($search) {

        //           $query->where('products.name', 'ILIKE', '%' . $search . '%')

        //             ->orWhere('products.description', 'ILIKE', '%' . $search . '%');
        //     });
        // }
        // return $paginatedProducts;
        // dd(DB::getQueryEnable())


    }
}
