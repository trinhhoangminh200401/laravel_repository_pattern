<?php
namespace App\Repositories\Repository;

use App\Interface\IRepository\IComboRepository;
use App\Models\Combo;
use App\Models\ComboProduct;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ComboRepository extends BaseRepository implements IComboRepository{

    public function getModel(): string
    {
        return Combo::class;
    }
    public function getAllCombos($filters = [], $limit = '', $page = '', $options = [])
    {

         $query= $this->getAll();

        if(!empty($options['select'])){

            $query->select($options['select']);
        }
        if(!empty($filters['search'])){

            $search=$filters['search'];

            $query= $query->where(function ($q) use ($search){
                    $q->where('name','like', "%{$search}%")
                      ->orWhere('description','like', "%{$search}%");

            });

        }
        if (!empty($filters)) {

            if (isset($filters['status'])) {

                $query->where('status', $filters['status']);
            }

            if (isset($filters['sort_by'])) {

                if (!empty(key($filters['sort_by']) == 'id')) {

                    $query->orderBy('id', $filters['sort_by']['id'] == 'asc' ? 'asc' : 'desc');
                }
            }
        }
        $offset = ($page - 1) * $limit;

        $totalRecords = $query->count();

        $totalPages = ceil($totalRecords / $limit);

        $nextPage = ($page < $totalPages) ? $page + 1 : null;

        $prevPage = ($page > 1) ? $page - 1 : null;

        $combos=$this->paginationData($offset,$limit,null,$query);

        return [
            'data' => $combos,

            'pagination' => [

                'totalRecords' => $query->count(),

                'totalPages' => $totalPages ?? null,

                'currentPage' => $page,

                'perPage' => $limit,

                'nextPage' =>  $nextPage,

                'prevPage' => $prevPage
            ]
        ];

    }

    public function createCombo($inputs = [])
    {
        try {
            DB::beginTransaction();

            $attributes = [
                'name' => $inputs['name'],
                'description' => $inputs['description'],
                'created_at' => now(),
                'updated_at' => now(),
                'images' => 'https://example.com/image.png',

                'status'=>'actived'
            ];

            $combo=$this->create($attributes);

            foreach($inputs['product_ids'] as $value){

                DB::table('combo_products')->insert([

                        'product_id'=>$value,

                        'combo_id'=>$combo->id,

                        'created_at' => now(),

                        'isDeleted' => 00,

                        'updated_at' => now(),

                ]);

            }
            DB::commit();

            return true;

        } catch (\Exception $err) {

            DB::rollBack();

            Log::info($err->getMessage());

            return false;
        }
    }

}

?>
