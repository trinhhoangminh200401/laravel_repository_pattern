<?php

namespace App\Repositories\Repository;

use App\Interface\IRepository\IOrderRepository;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderRepository extends  BaseRepository implements IOrderRepository
{
    public function getModel(): string
    {
        return Order::class;
    }
    public function getAllOrder($options = [] ,$limit = '', $page = '')
    {
            $query = $this->model->newQuery();
        $query->select('orders.*');

        if (!empty($options['left_join_order_combo'])) {
            $subQueryCombo = DB::table('order_combo')
            ->select('order_id', DB::raw('SUM(quantity) as total_quantity'))
            ->groupBy('order_id');

            $query->leftJoinSub($subQueryCombo, 'oc', function ($join) {
                $join->on('orders.id', '=', 'oc.order_id');
            })

            ->addSelect(DB::raw(
                'COALESCE(oc.total_quantity, 0) AS order_quantity_combo'
            ));
            $query->groupBy('oc.total_quantity');
        }

        if (!empty($options['left_join_order_items'])) {
            $subQueryItems = DB::table('order_items')
            ->select('order_id', DB::raw('SUM(quantity) as total_quantity'))
            ->groupBy('order_id');

            $query->leftJoinSub($subQueryItems, 'oi', function ($join) {
                $join->on('orders.id', '=', 'oi.order_id');
            })
            ->addSelect(DB::raw(
                'COALESCE(oi.total_quantity, 0) AS order_quantity_product'
            ));
             $query->groupBy('oi.total_quantity');
        }

        if (!empty($options['left_join_order_combo']) || !empty($options['left_join_order_items'])) {
            $query->groupBy('orders.id','orders.customer_id');
        }

        $offset = ($page - 1) * $limit;
        $totalCount = $query->count();
        $totalPages = ceil($totalCount / ($limit ?? 1));
        $nextPage = ($page < $totalPages) ? $page + 1 : null;
        $prevPage = ($page > 1) ? $page - 1 : null;

        $data = $query->where('orders.isDeleted', '0')
        ->offset($offset)
        ->limit($limit)
        ->get();

        return [
            'data' => $data,
            'pagination' => [
                'totalRecords' => $totalCount,
                'totalPages' => $totalPages,
                'currentPage' => $page,
                'perPage' => $limit,
                'nextPage' => $nextPage,
                'prevPage' => $prevPage,
            ],
        ];

    }

    //  this code is create order when someone
    public function createOrder($inputs=[]){
        // dd($inputs);
        try {
            DB::beginTransaction();

            $order = Order::create([
                'customer_id' => $inputs['customer_id'],
                'total_price' => 0,
                'status'=>'pending'
            ]);


            $totalPrice = 0;
            if (!empty($inputs['products'])) {

                foreach ($inputs['products'] as  $product) {

                    $order->items()->create([

                        'order_id'=>$order->id,

                        'product_id' => $product['product_id'],

                        'quantity'=>$product['quantity'],

                        'price'=>$product['price'],


                    ]);
                    // dd($order->items());
                    $totalPrice += $product['quantity'] * $product['price'];

                }

            }
            if(!empty($inputs['combos'])){

                 foreach ($inputs['combos'] as $combo){
                    $data=[

                        'order_id' => $order->id,

                        'isDeleted' => 0,

                        'combo_id' => $combo['combo_id'],

                        'quantity' => $combo['quantity'],

                        'price' => $combo['price'],

                    ];

                    $order->combos()->create($data);

                 }
                 $totalPrice += $combo['quantity']*$combo['price'];

            }
            $order->update(['total_price' => $totalPrice]);
            DB::commit();
            return $order;


        } catch (\Exception $err) {
            DB::rollBack();
            Log::info($err->getMessage());

            return false;

        }


    }
    public function destroyOrder(){
        try {

        } catch (\Throwable $th) {

        }

    }
}
