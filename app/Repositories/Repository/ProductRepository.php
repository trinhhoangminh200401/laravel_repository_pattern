<?php

namespace App\Repositories\Repository;

use App\Http\constant\Util;
use App\Interface\IRepository\IProductRepository;
use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ProductRepository extends BaseRepository implements IProductRepository
{
    public function getModel():string
    {

        return Product::class;

    }

    public function getProductWithType(): Collection
    {
        return $this->model->with('product')->get();
    }
    // public function getList($conditions=[], $options=[])
    // {


    //     $query = $this->model->query();
    //     return $this->searchOptions($query, $options);

    // }


    // public function getLimitProduct():Collection
    // {

    //     return $this->model->limit(2)->get();

    // }
    // ProductRepository.php

    // ProductRepository.php

    public function getAllProducts($filters = [], $limit = '', $page = '',$options=[])
    {


        $query = $this->getAll();

        if(!empty($options['select'])){
            $query->select($options['select']);
        }

        if (!empty($options['with'])) {
            foreach ($options['with'] as $key => $value) {
                if(is_callable($value)){
                    $query->with([$key=>$value]);
                }else{
                    $query->with([$value]);

                }
            }
        }

        if (!empty($filters['search'])) {

            $search = $filters['search'];

            $query->where(function ($q) use ($search) {

                $q->where('name', 'LIKE', "%{$search}%")

                ->orWhere('description', 'LIKE', "%{$search}%");
            });
        }

        if(!empty($filters['status'])){
            $query->where('product_type_id', $filters['status']['value']);
        }

        $offset = ($page - 1) * $limit;

        $totalPages = $query->count() / ($limit ?? 1);

        $nextPage = ($page < $totalPages) ? $page + 1 : null;

        $prevPage = ($page > 1) ? $page - 1 : null;


        $products = $this->paginationData($offset, $limit,null ,$query);


        return [
            'data' => $products,

            'pagination' => [

                'totalRecords' => $query->count(),

                'totalPages' =>$totalPages ?? null ,

                'currentPage' => $page,

                'perPage' => $limit,

                'nextPage'=>  $nextPage,

                'prevPage'=> $prevPage
            ]
        ];
    }

    public function getTopProducts($filters = [], $limit = '', $page = '',$options=[])
    {
        // Temporarily
        $query = $this->getAll();
        if($options['with'] && $options['with']['listImage']){
            $query->with('listImage');
        }

        if (!empty($filters['search'])) {

            $search = $filters['search'];

            $query->where(function ($q) use ($search) {

                $q->where('name', 'LIKE', "%{$search}%")

                    ->orWhere('description', 'LIKE', "%{$search}%");
            });
        }

        if(!empty($filters['status'])){
            $query->where('product_type_id', $filters['status']['value']);
        }

        $offset = ($page - 1) * $limit;

        $products = $this->paginationData($offset, $limit,null ,$query);
        $products->makeHidden(["updated_at", "created_at","isDeleted","status",'list_image']);
        $totalPages= $products->count() / ($limit ?? 1);

        $nextPage = ($page < $totalPages) ? $page + 1 : null;

        $prevPage = ($page > 1) ? $page - 1 : null;

        return [
            'data' => $products,

            'pagination' => [

                'totalRecords' => $query->count(),

                'totalPages' =>$totalPages ?? null ,

                'currentPage' => $page,

                'perPage' => $limit,

                'nextPage'=>  $nextPage,

                'prevPage'=> $prevPage
            ]
        ];
    }


}
