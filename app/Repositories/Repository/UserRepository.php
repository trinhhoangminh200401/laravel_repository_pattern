<?php

namespace App\Repositories\Repository;

use App\Interface\IRepository\IUserRepository;
use App\Models\Customer;
use Illuminate\Support\Facades\DB;

class UserRepository extends BaseRepository implements IUserRepository
{
public function getModel(): string
 {
        return Customer::class;

 }

    public function  getAllUsers($filters=[],$limit='',$page='',$options=[]){

        $query = $this->getAll();


        if(!empty($options['with'])){
            foreach ($options['with'] as $key => $value) {
                 if(is_callable($value)){
                    $query->with([$key=>$value]);
                 }
                 else {
                    $query->with($value);
                 }
            }
        }
        if(!empty($options['select'])){
            $query->select($options['select']);
        }
        if (!empty($filters['search'])) {

            $search = $filters['search'];

            $query->where(function ($q) use ($search) {

                $q->where('name', 'LIKE', "%{$search}%")

                ->orWhere('email', 'LIKE', "%{$search}%")

                ->orWhere('phone_number', 'LIKE', "%{$search}%");
            });
        }
        if (isset($filters['status']) && $filters['status']['value']!=null) {

            $query->where('isBlocked', $filters['status']['value']);

        }
        $offset = ($page - 1) * $limit;
        $totalPages =  $query->count() / ($limit ?? 1);

        $nextPage = ($page < $totalPages) ? $page + 1 : null;

        $prevPage = ($page > 1) ? $page - 1 : null;
        $users = $this->paginationData($offset,$limit,null,$query);



        return [
            'data' => $users,

            'pagination' => [

                'totalRecords' => $users->count(),

                'totalPages' => $totalPages ?? null,

                'currentPage' => $page,

                'perPage' => $limit,

                'nextPage' =>  $nextPage,

                'prevPage' => $prevPage
            ]

        ];

    }
    public function findByEmail($email)
    {

        return $this->model->where('email', '=', $email)->first();
    }
    public function blockUser($id)
    {
        $currentUser = $this->model->find($id);

        if (!$currentUser) {

            return false;
        }
        if ($currentUser && $currentUser->isDeleted != 1) {

            $currentUser->isBlocked = 1;

            $currentUser->updated_at = now();

            $currentUser->save();

            return true;
        }

        return false;
    }
    public function  blockMultiple($request)
    {
        $this->model->whereIn('id', $request->input('ids',[]))

            ->where('isDeleted', '!=', 1)

            ->update([

                'isBlocked' => 1,

                'updated_at' => now()
            ]);
        return true;
    }
}
