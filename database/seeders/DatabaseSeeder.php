<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\ProductType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // $productType =[
        //     [
        //         'name'=>'pizza',
        //         'image'=>'null',
        //         'created_at' => now(),
        //         'updated_at' => now(),
        //     ],
        //     [
        //         'name'=>'spaggeti',
        //         'image'=>'null',
        //         'created_at' => now(),
        //         'updated_at' => now(),
        //     ]
        // ];
        // foreach ($productType as $product) {
        //     ProductType::create($product);
        // }

        $this->call(
            FoodSeeder::class,
        );
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
