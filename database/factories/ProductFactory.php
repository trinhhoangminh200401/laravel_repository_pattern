<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    public function definition(): array
    {
        return [

            'name' => $this->faker->word(),

            'product_type_id' => $this->faker->numberBetween(1, 10),

            'description' => $this->faker->paragraph(),

            'price' => $this->faker->numberBetween(1500, 6000),

            'amount' => $this->faker->numberBetween(1, 100),

            'size' => $this->faker->randomElement(['S', 'M', 'L', 'XL']),

            'image' => $this->faker->imageUrl(640, 480, 'products', true),

            'isDeleted' => 0,

            'status' => $this->faker->randomElement(['actived', 'disabled']),

            'created_at' => now(),

            'updated_at' => now(),

        ];
    }
}
