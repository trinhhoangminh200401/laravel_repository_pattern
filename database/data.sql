-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               PostgreSQL 16.2, compiled by Visual C++ build 1937, 64-bit
-- Server OS:
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES  */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table public.tbl_combo_products
DROP TABLE IF EXISTS "tbl_combo_products";
CREATE TABLE IF NOT EXISTS "tbl_combo_products" (
	"id" BIGINT NOT NULL DEFAULT 'nextval(''tbl_combo_products_id_seq'')',
	"name" VARCHAR(255) NOT NULL,
	"description" TEXT NULL DEFAULT NULL,
	"created_at" TIMESTAMP NULL DEFAULT NULL,
	"updated_at" TIMESTAMP NULL DEFAULT NULL,
	"isDeleted" SMALLINT NULL DEFAULT '0',
	"image" TEXT NULL DEFAULT NULL,
	PRIMARY KEY ("id")
);

-- Dumping data for table public.tbl_combo_products: -1 rows
/*!40000 ALTER TABLE "tbl_combo_products" DISABLE KEYS */;
INSERT INTO "tbl_combo_products" ("id", "name", "description", "created_at", "updated_at", "isDeleted", "image") VALUES
	(1, 'combo 1', 'Gà rán + nước +pizza', '2024-09-24 14:26:58', '2024-10-04 06:49:40', 1, NULL),
	(2, 'combo 2', 'Pizza + nước', '2024-09-24 14:27:00', '2024-10-04 06:50:22', 1, NULL);
/*!40000 ALTER TABLE "tbl_combo_products" ENABLE KEYS */;

-- Dumping structure for table public.tbl_product
DROP TABLE IF EXISTS "tbl_product";
CREATE TABLE IF NOT EXISTS "tbl_product" (
	"id" BIGINT NOT NULL DEFAULT 'nextval(''table_product_id_seq''::regclass)',
	"name" VARCHAR(255) NOT NULL,
	"description" VARCHAR(255) NOT NULL,
	"price" DOUBLE PRECISION NOT NULL,
	"created_at" TIMESTAMP NULL DEFAULT NULL,
	"updated_at" TIMESTAMP NULL DEFAULT NULL,
	"amount" INTEGER NULL DEFAULT NULL,
	"size" VARCHAR(60) NULL DEFAULT 'NULL::character varying',
	"image" TEXT NULL DEFAULT NULL,
	"product_type_id" BIGINT NULL DEFAULT NULL,
	"isDeleted" SMALLINT NULL DEFAULT '0',
	"combo_id" BIGINT NULL DEFAULT NULL,
	PRIMARY KEY ("id"),
	INDEX "idx_price" ("price")
);

-- Dumping data for table public.tbl_product: 14 rows
/*!40000 ALTER TABLE "tbl_product" DISABLE KEYS */;
INSERT INTO "tbl_product" ("id", "name", "description", "price", "created_at", "updated_at", "amount", "size", "image", "product_type_id", "isDeleted", "combo_id") VALUES
	(14, 'Pizza', 'Nihil id placeat consequatur dolorem accusantium et. Autem a vel enim. Voluptatem pariatur accusamus ut qui. Deserunt blanditiis animi cumque sequi.', 1, '2024-09-10 09:26:19', '2024-09-10 09:28:39', 1, '1', '[''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'']', 1, 0, 1),
	(7, 'Jaquan Schimmel II', 'Nihil id placeat consequatur dolorem accusantium et. Autem a vel enim. Voluptatem pariatur accusamus ut qui. Deserunt blanditiis animi cumque sequi.', 26111, '2024-09-10 08:58:32', '2024-09-10 09:46:40', 1, '1', '[''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'']', 2, 0, NULL),
	(9, 'Jaquan Schimmel II', 'Nihil id placeat consequatur dolorem accusantium et. Autem a vel enim. Voluptatem pariatur accusamus ut qui. Deserunt blanditiis animi cumque sequi.', 2651, '2024-09-10 09:16:43', '2024-09-10 09:46:40', 1, '1', '[''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'']', 1, 0, NULL),
	(12, 'Jaquan Schimmel II', 'Nihil id placeat consequatur dolorem accusantium et. Autem a vel enim. Voluptatem pariatur accusamus ut qui. Deserunt blanditiis animi cumque sequi.', 26111, '2024-09-10 09:20:22', '2024-09-10 09:46:40', 1, '1', '[''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'']', 1, 0, NULL),
	(2, 'Derick Schaefer', 'Pariatur et laborum qui in. At et tenetur quia et et sapiente ut repellendus. Iste dolore aut labore quae quas.', 1576, '2024-07-04 07:27:31', '2024-09-10 08:58:32', 1, '1', '[''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'']', 1, 0, NULL),
	(4, 'Prof. Camryn Thompson', 'Odio at voluptas eum quos laboriosam inventore. Vitae 1a voluptatum dolorem quo. Ut et ratione quo in. Natus maiores quos repudiandae ducimus magnam occaecati.', 1567, '2024-07-04 07:27:31', '2024-09-10 08:58:32', 1, '1', '[''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'']', 1, 0, NULL),
	(8, 'Dwight Conroy', 'Qui deleniti ratione voluptatem qui repudiandae. Beatae aut nam nostrum dolorem sequi. Quae aut temporibus voluptate rem est. Est repellendus doloribus sunt dignissimos aperiam nihil eius.', 369, '2024-09-10 09:01:56', '2024-09-10 09:28:39', 1, '1', '[''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'']', 1, 0, NULL),
	(1, 'Alex Jaskolski', 'Laboriosam ut accusantium cupiditate. Iusto iste incidunt quaerat saepe non id neque.', 42221, '2024-07-04 07:27:31', '2024-09-27 06:42:55', 1, '1', '[''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'']', 2, 1, NULL),
	(5, 'Jaquan Schimmel II', 'Nihil id placeat consequatur dolorem accusantium et. Autem a vel enim. Voluptatem pariatur accusamus ut qui. Deserunt blanditiis animi cumque sequi.', 26111, '2024-07-04 07:27:31', '2024-09-10 09:28:39', 1, '1', '[''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'']', 1, 0, NULL),
	(13, 'Conroysdad', 'Qui deleniti ratione voluptatem qui repudiandae. Beatae aut nam nostrum dolorem sequi. Quae aut temporibus voluptate rem est. Est repellendus doloribus sunt dignissimos aperiam nihil eius.', 396129, '2024-09-10 09:20:22', '2024-09-10 09:20:22', 1, '1', '[''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'']', 1, 0, NULL),
	(11, 'Dwight Conroysad', 'Qui deleniti ratione voluptatem qui repudiandae. Beatae aut nam nostrum dolorem sequi. Quae aut temporibus voluptate rem est. Est repellendus doloribus sunt dignissimos aperiam nihil eius.', 12122, '2024-09-10 09:20:22', '2024-09-10 09:20:22', 1, '1', '[''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'']', 1, 0, NULL),
	(6, 'Alex', 'Laboriosam ut accusantium cupiditate. Iusto iste incidunt quaerat saepe non id neque.', 4291, '2024-07-22 10:20:09', '2024-09-10 08:58:32', 1, '1', '[''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'']', 2, 0, NULL),
	(10, 'Conroy', 'Qui deleniti ratione voluptatem qui repudiandae. Beatae aut nam nostrum dolorem sequi. Quae aut temporibus voluptate rem est. Est repellendus doloribus sunt dignissimos aperiam nihil eius.', 3969, '2024-09-10 09:16:43', '2024-09-10 09:16:43', 1, '1', '[''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'']', 2, 1, NULL),
	(3, 'Dwight Conroy', 'Qui deleniti ratione voluptatem qui repudiandae. Beatae aut nam nostrum dolorem sequi. Quae aut temporibus voluptate rem est. Est repellendus doloribus sunt dignissimos aperiam nihil eius.', 369, '2024-07-04 07:27:31', '2024-09-10 09:46:40', 1, '1', '[''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'',''https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckDwKsSN5uj3qbhnPMyW3Aso3U6k9zX3CVHxv79chugH2K6dnHw&s'']', 1, 1, NULL);
/*!40000 ALTER TABLE "tbl_product" ENABLE KEYS */;

-- Dumping structure for table public.tbl_product_type
DROP TABLE IF EXISTS "tbl_product_type";
CREATE TABLE IF NOT EXISTS "tbl_product_type" (
	"id" BIGINT NOT NULL DEFAULT 'nextval(''product_types_id_seq''::regclass)',
	"name" VARCHAR(80) NULL DEFAULT NULL,
	"image" VARCHAR(255) NULL DEFAULT NULL,
	"created_at" TIMESTAMP NULL DEFAULT NULL,
	"updated_at" TIMESTAMP NULL DEFAULT NULL,
	"isDeleted" SMALLINT NULL DEFAULT '0',
	PRIMARY KEY ("id")
);

-- Dumping data for table public.tbl_product_type: -1 rows
/*!40000 ALTER TABLE "tbl_product_type" DISABLE KEYS */;
INSERT INTO "tbl_product_type" ("id", "name", "image", "created_at", "updated_at", "isDeleted") VALUES
	(1, 'pizza', 'null', '2024-09-12 07:53:50', '2024-09-12 07:53:50', 0),
	(2, 'spaggeti', 'null', '2024-09-12 07:53:50', '2024-09-12 07:53:50', 0);
/*!40000 ALTER TABLE "tbl_product_type" ENABLE KEYS */;

-- Dumping structure for table public.users
DROP TABLE IF EXISTS "users";
CREATE TABLE IF NOT EXISTS "users" (
	"id" INTEGER NOT NULL DEFAULT 'nextval(''users_id_seq''::regclass)',
	"name" VARCHAR(255) NOT NULL,
	"email" VARCHAR(255) NOT NULL,
	"password" VARCHAR(255) NOT NULL,
	"created_at" TIMESTAMP NULL DEFAULT NULL,
	"updated_at" TIMESTAMP NULL DEFAULT NULL,
	"role" VARCHAR(255) NOT NULL DEFAULT 'user',
	"phone_number" VARCHAR(255) NULL DEFAULT 'NULL::character varying',
	"delivery_address" VARCHAR(255) NULL DEFAULT 'NULL::character varying',
	"isDeleted" SMALLINT NULL DEFAULT '0',
	"isBlocked" SMALLINT NULL DEFAULT '0',
	"isVerified" SMALLINT NULL DEFAULT '0',
	"image" VARCHAR(255) NULL DEFAULT 'NULL::character varying',
	PRIMARY KEY ("id"),
	UNIQUE INDEX "users_email_key" ("email")
);

-- Dumping data for table public.users: -1 rows
/*!40000 ALTER TABLE "users" DISABLE KEYS */;
INSERT INTO "users" ("id", "name", "email", "password", "created_at", "updated_at", "role", "phone_number", "delivery_address", "isDeleted", "isBlocked", "isVerified", "image") VALUES
	(1, 'Minh', 'uchihasasuke0122356@gmail.com', '$2y$12$njDN4c5kDH1cJusDIrEUmOJs3eLf5OnJaKZA1XJPgZTnQU3L4EOHS', '2024-07-11 08:55:27', '2024-10-07 07:04:02', 'admin', NULL, NULL, 0, 0, 1, NULL),
	(7, 'Minh', 'hoangminh200401@gmail.com', '$2y$12$dpJ1BFF6eu8XFmdjoiT/muvUwOR/sXCulv4Bz/mfDewSsABikqv.6', '2024-10-01 07:15:23', '2024-10-07 07:04:02', 'user', NULL, NULL, 0, 0, 1, NULL),
	(9, 'Trung', 'minhtrung4367@gmail.com', '$2y$12$6gt0wYW16uDu/IXXpQqBce23jXf25Pp62fEQ8IbtucXrn.PtQjexq', '2024-10-01 16:04:27', '2024-10-01 16:06:41', 'user', NULL, NULL, 0, 0, 0, NULL),
	(10, 'Khuong', 'phamduykhuong1412@gmail.com', '$2y$12$FhDUSC3ytzzCg3/TV6yhru1litZZlMfsFqSu0LvVk4qq3WozBqZqy', '2024-10-04 08:56:27', '2024-10-04 08:58:02', 'user', NULL, NULL, 0, 1, 1, NULL),
	(11, 'Vân Quan', 'quanbichvan1@gmail.com', '$2y$12$0C2rjSe/ERPZyttgx3rYQuYta.Ka2xwvXF7/WY8TpOXk8U9cp25PO', '2024-10-05 16:11:03', '2024-10-05 16:46:30', 'user', NULL, NULL, 0, 0, 1, NULL);
/*!40000 ALTER TABLE "users" ENABLE KEYS */;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
