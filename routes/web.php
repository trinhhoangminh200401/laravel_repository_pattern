<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CsvController;
use App\Http\Controllers\ProductController;
use App\Models\Product;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::post('/exportProduct', [CsvController::class, 'exportFileProductCsv'])->name('exportFileProductCsv');

Route::post('/importProduct', [CsvController::class, 'importProductCsv'])->name('importFileProductCsv');

Route::post('/exportType', [CsvController::class, 'exportFileTypeCsv'])->name('exportFileTypeCsv');

Route::post('/importType', [CsvController::class, 'importTypeCsv'])->name('importFileTypeCsv');


// Route::get('/login', function () {
//     return response()->json(['message' => 'Login route not implemented'], 401);
// })->name('login');


Route::get('/', function () {
    return view('csv');
})->name('csv');
