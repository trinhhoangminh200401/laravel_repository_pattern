<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\AuthAdminController;
use App\Http\Controllers\ComboController;
use App\Http\Controllers\ComboProductController;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductTypeController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::controller(AuthController::class)->middleware('cors')->group(function () {

    Route::get('profile', 'profile');

    Route::post('login', 'login')->middleware(['block']);

    Route::post('register', 'register');

    Route::post('logout', 'logout');

    Route::post('refreshToken', 'refreshToken');

    Route::post('verifyOtp',  'verifyOtpEmail');

    Route::get('auth/google', 'redirectToGoogle');

    Route::get('auth/callback', 'handleGoogleCallback');

    Route::get('auth/facebook', 'redirectToFacebook');

    Route::get('auth/facebook/callback', 'handleFacebookCallback');
});


/* -----------------------------PRODUCT ROUTER-------------------------------*/

Route::controller(ProductController::class)->middleware(['cors'])->prefix('/product')->group(function () {

    Route::get('', 'index');
    Route::get('/topProduct', 'topProduct');

    Route::get('/productType', 'getProductWithType');

    Route::get('/id={id}', 'getProductId');

});
/* -----------------------------PAYMENT ROUTER-------------------------------*/

Route::controller(PaymentController::class)->middleware(['cors'])->prefix('/payment')->group(function(){

    Route::get('/vnpay', 'VnPayMethod');
    Route::get('/vnpay_url', 'returnUrl');


});
/*------------------------------Order ROUTER ---------------------------------*/

Route::controller(OrderController::class)->middleware(['cors'])->prefix('/order')->group(function(){

    Route::post('/insert', 'createOrder');

    Route::get('','getAllOrder');

});

/* -----------------------------CATEGORY ROUTER-------------------------------*/

Route::controller(ProductTypeController::class)->middleware(['cors'])->prefix('/productType')->group( function () {

    Route::get('','getProductType');

    Route::get('getTypeId','getTypeId');
    Route::get('/productBelong', 'getProductsByType');
    Route::put('/multipleDeleteType', 'mutipleDeleteProductType');


});
/* -----------------------------COMBOPRODUCT ROUTER-------------------------------*/

Route::controller(ComboController::class)->middleware(['cors'])->prefix('/combo')->group(function(){

    Route::get('','index');

    Route::post('/createCombo', 'store');

//     Route::get('/detail','getComboDetail');

//     Route::get('/combo','getComboId');

//     Route::get('/comboProductPage','getPageData')->where('page','[0-9]+');

//     Route::put('/destroy/{comboProduct}','destroyComboProduct');
});
/* -----------------------------customer  ROUTER-------------------------------*/

Route::controller(UserController::class)->prefix('/user')->middleware('cors')->group(function () {
    Route::middleware('auth:api')->group(function () {
        Route::put('/updated/{id}', 'updateUserData');
        Route::get('/id', 'getUserId');
        // Route::put('/update/{id}', 'updateUserData');
    });



    // Route::post('/uploadImageUser', 'uploadUser');
    // Route::post('/uploadImageProduct', 'uploadProduct');
});
// Route::controller(UserController::class)->middleware(['auth:api', 'auth.role:admin', 'cors'])->prefix('/userManage')->group(function () {



//     Route::put('/block/{id}', 'blockCurrentUser');

//     Route::post('/blockMultiple', 'blockMultipleUser');
// });

/* -----------------------------ADMIN ROUTER-------------------------------*/

Route::group(['prefix' => 'admin', 'middleware' => ['cors']], function () {
    Route::post('login', [AuthAdminController::class, 'login']);
    Route::post('refreshToken', [AuthAdminController::class, 'refreshToken']);

    Route::middleware('auth.role:admin,staff')->group(function () {
        Route::get('profile', [AuthAdminController::class, 'profile']);
        Route::post('logout', [AuthAdminController::class, 'logout']);
        Route::apiResource('staff', StaffController::class);


        Route::controller(ProductController::class)->prefix('/productManage')->group(function () {

            Route::post('/post-food', 'insertProduct');

            Route::put('/update/{id}', 'updateProduct');

            Route::put('/destroy/{id}', 'destroyProductId');

            Route::delete('/delete/{id}', 'deleteProductId');
        });

    });
    Route::controller(UploadController::class)->prefix('/upload')->group(function(){
        Route::post('','uploadController');
    });
    Route::controller(UserController::class)->prefix('/userManage')->group(function () {
        Route::get('/id', 'getUserId');

        Route::get('', 'getAllUser');
        // Route::put('/update/{id}', 'updateUserData');

        Route::put('/block/{id}', 'blockCurrentUser');

        Route::put('/blockMultiple', 'blockMultipleUser');
    });

});



