<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Export CSV</title>
    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container mt-5">
        @if (session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
      </div>

   @endif
        <h1 class="text-center">Export Data to CSV</h1>
        <div class="row justify-content-center mt-4">

            <div class="col-md-6 text-center">

                <form action="{{ route('exportFileProductCsv') }}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-primary">Export CSV</button>
                </form>
                <form action="{{ route('importFileProductCsv') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="file">Upload CSV File:</label>
                        <input type="file" name="importCsvFile" id="file" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Import CSV</button>
                </form>


            </div>
        </div>
    </div>
     <div class="row justify-content-center mt-4">

            <div class="col-md-6 text-center">

                <form action="{{ route('exportFileTypeCsv') }}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-primary">Export CSV</button>
                </form>
                <form action="{{ route('importFileTypeCsv') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="file">Upload CSV File:</label>
                        <input type="file" name="importCsvTypeFile" id="file" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Import CSV</button>
                </form>


            </div>
        </div>
    </div>
    <!-- Bootstrap JS and dependencies -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>

</html>
