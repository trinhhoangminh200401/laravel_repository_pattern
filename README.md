# Project: SHOPBALO

Github Project: https://github.com/tanthuan031/SHOPBALO.git

## A. BackEnd: Laravel

- Composer : https://getcomposer.org/download/
- Laravel:11
- Php: 8.1


## B. Database: MySQL

- Xampp Server : https://www.apachefriends.org/

<h5>Class Diagram</h5>


## Setup Project

## 1.Back End

### 1.1 Install packages:

```
composer install
```

### 1.2 Setup .env in Laravel

```
cp .env.example .env
```

#### Edit your .env:

```
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1    // your host name
DB_PORT=5438        // mysql port
DB_DATABASE=food-booking-db // database name
DB_USERNAME=root      // your database username
DB_PASSWORD=   // your database password
```

#### Generate key for project:

```
php artisan key:generate
```

#### 1.3 Setup Database

```
php artisan migrate
php artisan db:seed
```

#### 1.4 Setup Storage

```
php artisan storage:link
```

### 2. Run Project
Here are the steps to resolve this issue:

Generate the JWT Secret Key: If the secret key is missing, you can generate one using the following Artisan command:
````
php artisan jwt:secret
````

This command will generate a new key and add it to your .env file as JWT_SECRET.
Check the .env File: Ensure that the .env file has the JWT_SECRET variable set correctly. It should look like this:
```angular2html
JWT_SECRET=your_generated_secret_key
```
Clear Config Cache: Sometimes Laravel caches the configuration, so even after setting the key, you might still encounter the error. Clear the config cache using:
```angular2html
php artisan config:clear
```
RUN PROJECT

```
php artisan serve
```

### 3. Show List Router

```php
php artisan route:list
```
### Set Up Model and Migration
Create Model
````angular2html
php artisan make:model ModelName
````

Create Controller
````angular2html
php artisan make:controller Name
````
Các Phương Thức CRUD
1. index(): Lấy danh sách tất cả các bản ghi.
2. store(): Tạo một bản ghi mới.
3. show($id): Lấy thông tin chi tiết của một bản ghi cụ thể.
4. update($id): Cập nhật thông tin của một bản ghi cụ thể.
5. destroy($id): Xóa một bản ghi cụ thể.

NOTE: khi tao Service va Repository va IRepository ta phai add vao **DalServiceProvider**
## C. SET UP DOCKER
### Yêu cầu

- Docker đã được cài đặt trên máy.
- Đã đăng ký tài khoản Docker Hub và đăng nhập bằng lệnh `docker login`.

### Thực Hành
**Đăng nhập vào Docker Hub:** Trước tiên, bạn cần đảm bảo đã đăng nhập vào Docker Hub. Chạy lệnh sau để đăng nhập:

````
docker login
````

- Khi chạy lệnh này, Docker sẽ yêu cầu nhập username và password của bạn trên Docker Hub. Đảm bảo bạn nhập đúng thông tin tài khoản.
- Kiểm tra Tên Image: Kiểm tra lại tên image bạn đang push, đảm bảo rằng nó tuân theo định dạng username/image-name:tag. Trong trường hợp của bạn:
  **Commit Docker :**

- Image name: **minhtrung0110/db-food-app**

- Tag: latest
  Nếu bạn đã commit image với tên khác, bạn cần sửa lại tag trước khi push:
````
docker tag db-food-app minhtrung0110/db-food-app:latest
````
**Push Lại Image: ** Sau khi đảm bảo bạn đã đăng nhập và image có tên đúng, hãy thử push lại:
````
docker push minhtrung0110/db-food-app:latest
````

### Cách pull Docker image từ Docker Hub

Chạy lệnh sau để pull image PostgreSQL đã được cấu hình từ Docker Hub về máy:

```bash
docker pull minhtrung0110/db-food-app:latest
````

### Cách chạy container từ image
Sau khi pull image, bạn có thể chạy container với cấu hình PostgreSQL bằng lệnh:
````
docker run --name db-food-app -p 5438:5432 -d minhtrung0110/db-food-app:latest
````
- 5438:5432: Mở cổng 5438 trên máy local và map với cổng 5432 của container PostgreSQL.
- -d: Chạy container dưới dạng detached (chạy ngầm).
### Cập nhật và push lại image
Commit container thành image mới
Sau khi thực hiện các thay đổi trên container (ví dụ thêm dữ liệu, cập nhật database), bạn cần commit các thay đổi vào một image mới:

````
docker commit db-food-app minhtrung0110/db-food-app:latest

````
### Push image lên Docker Hub
Sau khi commit, bạn có thể push image mới lên Docker Hub để chia sẻ với team:

````
docker push minhtrung0110/db-food-app:latest
````

### Dừng và xóa container
Nếu bạn muốn dừng container:

````
docker stop db-food-app
````
### Để xóa container:

````
docker rm db-food-app
````

